// import 'package:path/path.dart';
// import 'package:sqflite/sqflite.dart';
// import 'dart:io' as io;
// import 'dart:async';
// import 'package:path_provider/path_provider.dart';
// import 'package:espe/model/token.dart';
// class DBProvider  {
//   // Database db;
//   // String _path;

//   // Future open({String dbName : 'todome.db'}) async {
//   // var databasesPath = await getDatabasesPath();
//   // _path = join(databasesPath, 'demo.db');


//   //   db = await openDatabase(_path, version: 1,
//   //       onCreate: (Database db, int version) async {
//   //         await db.execute('''
//   //         create table todo ( 
//   //           id integer primary key autoincrement, 
//   //           title text not null,
//   //           done integer not null,
//   //           created_at text not null,
//   //           updated_at text not null)
//   //       ''');
//   //   });
//   // }
//   // Future close() async => db.close();
//   static Database db_instance;

//   Future<Database> get db async {
//     if (db_instance != null)
//     return db_instance;

//     // if _database is null we instantiate it
//     db_instance = await initDB();
//     return db_instance;
//   }
//   initDB() async {
//     io.Directory documentsDirectory = await getApplicationDocumentsDirectory();
//     String path = join(documentsDirectory.path, "espe.db");
//     var db = await openDatabase(path, version: 1, onCreate: (Database db, int version) async {
//       await db.execute('CREATE TABLE Token(token TEXT);');
//     });
//     return db;
//   }
//   //add data
//   void addNewToken(Token token) async{
//     var db_connection = await db;
//     String query = 'INSERT INTO Token(title) VALUES(\'${token.token}\')';
//     await db_connection.transaction((transaction) async{
//       return await transaction.rawInsert(query);
//     });
//   }
//   //get data
//   Future<List<Token>> getToken() async{
//     var db_connection = await db;
//     List<Map> list = await db_connection.rawQuery('SELECT * FROM Token');
//     List<Token> token = new List();
//     for(int i =0;i<list.length;i++){
//       Token token = new Token();
//       token.token = list[i]['token'];
//     }
//     return token;
//   }  
// }
