import 'package:flutter/material.dart';
import 'package:espe/components/inputFields.dart';
import 'package:validators/validators.dart';
class CmForm extends StatelessWidget {
  final loginkey;
  final msgOnSaved;

  CmForm({@required this.loginkey,this.msgOnSaved});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Column(
        children: <Widget>[
          new Form(
            key: loginkey,
            child: new Column(
              children: <Widget>[
                new InputFields(
                  validator: (String value){
                    if(value.length < 1){
                      return 'معتبر نیست';
                    }
                  },
                  hint: 'پیام شما ',
                  obscure: false,
                  lines: 6,
                  onSaved :msgOnSaved,
                  type: TextInputType.text,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}