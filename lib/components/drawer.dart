import 'dart:convert';

import 'package:espe/pages/aboutus.dart';
import 'package:espe/pages/contactus.dart';
import 'package:espe/pages/freeService.dart';
import 'package:espe/pages/friends.dart';
import 'package:espe/pages/learnV1.dart';
import 'package:espe/pages/login.dart';
import 'package:espe/pages/prevUserSetting.dart';
import 'package:espe/pages/userHome.dart';
import 'package:espe/pages/userSetting.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'dart:io' as io;

import 'package:shared_preferences/shared_preferences.dart';
var name = '';
Drawer buildDrawerLayout(BuildContext context,[String name = 'نام کاربری']){
  return new Drawer(
    elevation: 0.0,
    child:new ListView(
      children:<Widget>[
        new DrawerHeader(
        padding: EdgeInsets.only(bottom: 25),
        child:
        // new Container(
        //   width: 50,
        //   height: 50,
        //   decoration: new BoxDecoration(
        //     image: new DecorationImage(
        //       image: new AssetImage('assets/images/un.png'),

        //     )
        //   ),
        // )
        new GestureDetector(
          child:new Stack(
          children: <Widget>[
            new Container(
            width: 170,
            height: 170,
            padding: EdgeInsets.fromLTRB(25, 25, 25, 30),
            child:Image.asset('assets/images/un.png'),
            ),
                new Align(
                  alignment: Alignment.bottomCenter,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Text(name,style: new TextStyle(color: Color(0xff36b07a),fontSize: 17)),
                      new Icon(Icons.edit,size: 17,color:Color(0xff36b07a),),
                    ],
                  ),
                ),
            ],
          ),
         onTap: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:PrevUserSetting() ,)));
          },
        )
      ),
        // new ListTile(
        //   dense: true,
        //   title: new Text('خانه',textAlign: TextAlign.right,style: TextStyle(color:Color(0xff36b07a),fontSize: 15),),
        //   onTap: (){
        //  Navigator.push(
        //  context,
        //  new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:UserHome() ,)));
        //   },
        // ),
        new ListTile(
          dense: true,
          title: new Text('خدمات رایگان',textAlign: TextAlign.right,style: TextStyle(color:Color(0xff36b07a),fontSize: 15),),
          onTap: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:FreeServiceView() ,)));
          },
        ),
        new ListTile(
          dense: true,
          title: new Text('آموزش',textAlign: TextAlign.right,style: TextStyle(color:Color(0xff36b07a),fontSize: 15),),
         onTap: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:LearnV1() ,)));
          },
        ),
        new ListTile(
          dense: true,
          title: new Text('درباره ما',textAlign: TextAlign.right,style: TextStyle(color:Color(0xff36b07a),fontSize: 15),),
         onTap: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:AboutUsView() ,)));
          },
        ),
        new ListTile(
          dense: true,
          title: new Text('ارتباط با ما',textAlign: TextAlign.right,style: TextStyle(color:Color(0xff36b07a),fontSize: 15),),
         onTap: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:ContactUsView() ,)));
          },
        ),
        new ListTile(
          dense: true,
          title: new Text('معرفی به دوستان',textAlign: TextAlign.right,style: TextStyle(color:Color(0xff36b07a),fontSize: 15),),
         onTap: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:FriendsView())));
          },
        ),
        new ListTile(
          dense: true,
          title: new Text('خروج از حساب ',textAlign: TextAlign.right,style: TextStyle(color:Color(0xff36b07a),fontSize: 15),),
         onTap: (){
           showDialog(
             context: context,
             builder: (BuildContext context){
               return AlertDialog(
                 title: Directionality(child: new Text('خروج',),textDirection: TextDirection.rtl,),
                 content: new Text('آیا میخواهید از حساب کاربری خود خارج شوید؟',textAlign: TextAlign.right,),
                 actions: <Widget>[
                   new FlatButton(
                     child: new Text('بله'),
                     onPressed: (){
                          deletetoken();
                          Navigator.pushReplacementNamed(context,'login');
                      },
                   ),
                   new FlatButton(
                     child: new Text('خیر'),
                     onPressed: (){
                       Navigator.pop(context);
                     },
                   ),
                 ],
               );
             }
           );
         }
        ),
      ],
    ),
  );
}
deletetoken() async{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.clear();
}
  getUserActiveState() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('userToken');
    final response = await get('http://130.185.74.192:3000/api/v1/user/${apiToken}');
    print(apiToken);
    var responseBody = json.decode(response.body);
    if(!(responseBody['data'] == '' || responseBody['data'] == null)){
      name = responseBody['data']['name'];
    } 
  }