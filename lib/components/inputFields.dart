import 'package:flutter/material.dart';

class InputFields extends StatelessWidget {
  final String hint;
  final bool obscure;
  final int lines;
  final validator;
  final onSaved;
  final type;
  final enabled;
  InputFields({this.hint,this.obscure,this.lines,this.validator,this.onSaved,this.enabled,this.type});
  @override
  Widget build(BuildContext context) {
    return Container(
      
    margin: const EdgeInsets.symmetric(horizontal: 40,vertical: 10),
    child: new TextFormField(
      validator:validator,
      enabled: enabled,
      onSaved: onSaved,
      textAlign: TextAlign.center,
      obscureText: obscure,
      maxLines: lines,
      keyboardType: type,
      style:const TextStyle(
        color: Colors.white,
        
      ),
      decoration: InputDecoration(
       focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white,width: 2.0),
          borderRadius: BorderRadius.circular(25.0)
        ),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white,width: 2.0),
          borderRadius: BorderRadius.circular(25.0)
        ),
        hintText: hint,
        errorBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.red,width: 2.0),
          borderRadius: BorderRadius.circular(25.0)
        ),
        focusedErrorBorder:OutlineInputBorder(
          borderSide: BorderSide(color: Colors.red,width: 2.0),
          borderRadius: BorderRadius.circular(25.0)
        ),
        hintStyle: const TextStyle(color:Colors.white,fontSize: 15,),
        contentPadding: const EdgeInsets.only(top:12,bottom: 12,left: 15,right:15)
      ),
    ),
    );
  }
}