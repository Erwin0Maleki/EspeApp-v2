import 'package:flutter/material.dart';
import 'package:espe/components/inputFields.dart';
import 'package:validators/validators.dart';
class LoginForm extends StatelessWidget {
  final loginkey;
  final phoneOnSaved;
  final passwordOnSaved;
  LoginForm({@required this.loginkey,this.phoneOnSaved,this.passwordOnSaved});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Column(
        children: <Widget>[
          new Form(
            key: loginkey,
            child: new Column(
              children: <Widget>[
                Opacity(
                    child: new InputFields(
                    validator: (String value){
                      if(!isNumeric(value) || value.length < 11){
                        return 'شماره همراه وارد شده معتبر نیست';
                      }
                    },
                    hint: 'تلفن همراه (انگلیسی)',
                    obscure: false,
                    lines: 1,
                    onSaved :phoneOnSaved,
                    type: TextInputType.number,
                  ),
                  opacity: 0.8,
                ),
                new Opacity(
                  child: new InputFields(
                   validator: (String value){
                    if(value.length < 8){
                      return 'طول پسورد باید حداقل 8 کاراکتر باشد';
                    }
                  },
                  hint: "گذواژه (انگلیسی)",
                  obscure: true,
                  lines: 1,
                  onSaved:passwordOnSaved
                ),
                opacity: 0.8,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}