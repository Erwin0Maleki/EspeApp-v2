import 'package:flutter/material.dart';
import 'package:espe/components/inputFields.dart';
import 'package:validators/validators.dart';
class RegisterForm extends StatelessWidget {
  final registerkey;
  final phoneOnSaved;
  final passswordOnSaved;
  final subCodeOnSaved;
  final nameOnSaved;
  final addressOnSaved;
  RegisterForm({@required this.registerkey,this.phoneOnSaved,this.addressOnSaved,this.passswordOnSaved,this.nameOnSaved,this.subCodeOnSaved});
  @override
  Widget build(BuildContext context) {
    return Container(
      child: new Column(
        children: <Widget>[
          new Form(
            key: registerkey,
            child: new Column(
              children: <Widget>[
                new InputFields(
                  hint: 'نام و نام خانوادگی',
                  obscure: false,
                  lines: 1,
                  onSaved :nameOnSaved
                ),
                new InputFields(
                  validator: (String value){
                    if(!isNumeric(value) || value.length < 11){
                      return 'شماره همراه وارد شده معتبر نیست';
                    }
                  },
                  hint: "تلفن همراه (انگلیسی)",
                  obscure: false,
                  lines: 1,
                  onSaved :phoneOnSaved,
                  type: TextInputType.number,
                ),
                new InputFields(
                   validator: (String value){
                    if(value.length < 8){
                      return 'طول پسورد باید حداقل 8 کاراکتر باشد';
                    }
                  },
                  hint: "گذواژه (انگلیسی)",
                  obscure: true,
                  lines: 1,
                  onSaved :passswordOnSaved
                ),
                new InputFields(
                   validator: (String value){
                    if(value.length > 7){
                      return 'طول کد باید حداکثر 6 عدد باشد';
                    }
                  },
                  hint: "کد معرف(اگر دارید)",
                  obscure: false,
                  lines: 1,
                  onSaved :subCodeOnSaved
                ),
                new InputFields(
                  hint: "آدرس",
                  obscure: false,
                  lines: 3,
                   onSaved :addressOnSaved
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}