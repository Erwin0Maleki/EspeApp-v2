import 'package:flutter/material.dart';
import 'package:espe/components/inputFields.dart';
import 'package:validators/validators.dart';
class UserInfoForm extends StatelessWidget {
  final userInfoKey;
  final weightOnSaved;
  UserInfoForm({@required this.userInfoKey,this.weightOnSaved});
  @override
  Widget build(BuildContext context) {
    return  Container(
      child: new Column(
        children: <Widget>[
          new Form(
            key: userInfoKey,
            child: new Column(
              children: <Widget>[
                new InputFields(
                  validator: (String value){
                    if(!isNumeric(value)){
                      return 'وزن وارد شده معتبر نیست';
                    }
                  },
                  hint: 'وزن (انگلیسی)',
                  obscure: false,
                  lines: 1,
                  onSaved :weightOnSaved
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}