import 'package:espe/pages/learnV1.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:espe/pages/splash.dart';
import 'package:espe/pages/login.dart';
// import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:espe/pages/userHome.dart';
import 'package:espe/pages/driverHome.dart';
import 'package:espe/pages/register.dart';
import 'package:espe/pages/userInfo.dart';
import 'package:espe/pages/home2.dart';
import 'package:espe/pages/freeService.dart';
import 'package:espe/pages/userSetting.dart';
import 'package:pushe/pushe.dart';
void main(){
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((_){
  runApp(Myapp());
  });
}

class Myapp extends StatefulWidget {
  @override
  _MyappState createState() => _MyappState();
}

class _MyappState extends State<Myapp> {
// final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
@override
  void initState() {
    // TODO: implement initState
    super.initState();
    // _firebaseMessaging.configure(
    //   onMessage: (Map<String, dynamic> message){
    //     print('onMessage:$message');
    //   },
    //   onLaunch: (Map<String, dynamic> message){
    //     print('onLaunch:$message');
    //   },
    //   onResume: (Map<String, dynamic> message){
    //     print('onResume:$message');
    //   }
    // );
    // _firebaseMessaging.getToken().then((String value){
    //   print("token:$value");
    // });
        Pushe.initialize(showDialog: true);
  }
@override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'ESPE',
      theme: ThemeData(
        fontFamily: 'IRANSans',
        cursorColor: Colors.white,
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      routes: {
        'home' : (context) => new Directionality(textDirection: TextDirection.rtl,child:UserHome() ,),
        'login' : (context) => new Directionality(textDirection: TextDirection.rtl,child:LoginView(),),
        'register' : (context) => new Directionality(textDirection: TextDirection.rtl,child:RegisterView(),),
        'splash' : (context) => new Directionality(textDirection: TextDirection.rtl,child:SplashView(),),
        'driver' : (context) => new Directionality(textDirection: TextDirection.rtl,child:DriverHome(),),
        'freeService' : (context) => new Directionality(textDirection: TextDirection.rtl,child:FreeServiceView(),),
        'learnV1' : (context) => new Directionality(textDirection: TextDirection.rtl,child:LearnV1(),),
      },
      debugShowCheckedModeBanner: false,
      home:new Directionality(textDirection: TextDirection.ltr,child:SplashView(),),
    );
  }
}