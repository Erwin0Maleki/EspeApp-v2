import 'package:espe/components/drawer.dart';
import 'package:espe/pages/learnV2.dart';
import 'package:espe/pages/userHome.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AboutUsView extends StatefulWidget {
  @override
  _AboutUsViewState createState() => _AboutUsViewState();
}

class _AboutUsViewState extends State<AboutUsView> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
     String nameD = '';
     var scoreT = '0';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getName();
  }
  Widget build(BuildContext context) {
    var page =MediaQuery.of(context).size;
    return new Scaffold(
    appBar: new AppBar(
      backgroundColor: Colors.white,
      title:Padding(
        padding: const EdgeInsets.only(left: 0),
        child: Center(child: Text('درباره ما',style: new TextStyle(color:Color(0xff36b07a)),)),
      ),
      
    ),
    key: _scaffoldKey,
    endDrawer: SizedBox(child: buildDrawerLayout(context,nameD),width: 170,),
    body:new Stack(
      children: <Widget>[
          new Material(
          child: new Container(
            color: Colors.red,
            child:new Container(
              height: page.height,
              width: page.width,
decoration: BoxDecoration(
               image: new DecorationImage(
                 image: new AssetImage("assets/images/espebg.png"),
                 fit: BoxFit.cover
               )
              ),
              child: ListView(
                padding: EdgeInsets.only(bottom: 100),
                children: <Widget>[
                  new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Container(
                width: 98,
                height: 98,
                margin: EdgeInsets.only(top: 60),
                decoration: BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/logoespe.png")
                  )
                ),
              ),
                new Padding(
                child: new Text('اِســپه',style: new TextStyle(fontSize: 20,color: Colors.white,fontWeight:FontWeight.bold)),
                padding: new EdgeInsets.fromLTRB(5,10,0,0)
              ),
           Container(
                 margin: EdgeInsets.fromLTRB(10, 40, 10, 0),
                 width: 370,
                 height: 200,
                 decoration: new BoxDecoration(
                   borderRadius: BorderRadius.circular(20),
                   color: Colors.white.withOpacity(0.3),
                 ),
                 child:Container(
                           width: 360,
                           padding: EdgeInsets.fromLTRB(10, 60, 10, 0),
                           child:Directionality(
                            child: Text('"اسپه" یک واژه اصیل مازنی و ایرانیست به معنای سپید، این گروه کاملا مردمی و غیر دولتی می کوشد تا با کمک دستان پرمهر همه ایرانیان حقیقی در نجات محیط زیست ایران زیبا سهمی داشته باشد.',
                             textAlign: TextAlign.right,
                             style: TextStyle(color: Colors.white,fontSize: 15.5,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                             ),textDirection: TextDirection.rtl,
                           ),
                         ),
               ),
            ],
          ), 
                ],
              )
              ),
            
                )
              ),
        new Positioned(
          child: new Container(
            margin: EdgeInsets.only(top:MediaQuery.of(context).size.height - 155),
            child: new Scaffold(
              backgroundColor: Colors.transparent,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: new Transform.scale(
        scale: 1.2,
      child:FloatingActionButton(
        backgroundColor:Color(0xff43d1c5),
        elevation: 5,
        child:new Icon(Icons.home,size: 28,),
        onPressed: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:UserHome() ,)));
        }
      ),
      ),
      bottomNavigationBar: new BottomAppBar(
          notchMargin: 16.0,
          shape: CircularNotchedRectangle(),
          color: Colors.white,
          elevation: 20,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween, //spacebetween if we have 2 children
            children: <Widget>[
                        Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: Text('${scoreT}:امتیاز',style: new TextStyle(color:Color(0xff36b07a)),)
                ),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: new IconButton(
                    icon: Icon(Icons.menu,color:Color(0xff36b07a)),
                    color:Color(0xff36b07a),
                    iconSize: 35,
                    onPressed: (){
                      _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                ),
             ],
          ),
        )
          ),
        )
        )
        ],
    ),
    );
  }
    getName() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      nameD = prefs.getString('userName');
      scoreT = prefs.getString('Score');
    });
  }
}