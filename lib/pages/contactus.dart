import 'package:espe/components/cmForm.dart';
import 'package:espe/components/drawer.dart';
import 'package:espe/pages/userHome.dart';
import 'package:flutter/material.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';

import 'package:shared_preferences/shared_preferences.dart';
class ContactUsView extends StatefulWidget {
  @override
  _ContactUsViewState createState() => _ContactUsViewState();
}

class _ContactUsViewState extends State<ContactUsView> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();
  String _msgValue;
  String nameD = '';
  var scoreT = '0';
  msgOnSaved(String value){
    print(value);
    _msgValue = value;
  }
    @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getName();
  }
  @override
  Widget build(BuildContext context) {
    var page =MediaQuery.of(context).size;
    return new Scaffold(
      resizeToAvoidBottomPadding: true,
    appBar: new AppBar(
      backgroundColor: Colors.white,
      title:Padding(
        padding: const EdgeInsets.only(left: 0),
        child: Center(child: Text('ارتباط با ما',style: new TextStyle(color:Color(0xff36b07a)),)),
      ),
    ),
    key: _scaffoldKey,
    endDrawer: SizedBox(child: buildDrawerLayout(context,nameD),width: 170,),
    body:new Stack(
      children: <Widget>[
          new Material(
          child: new Container(
            color: Colors.red,
            child:new Container(
              height: page.height,
              width: page.width,
decoration: BoxDecoration(
               image: new DecorationImage(
                 image: new AssetImage("assets/images/espebg.png"),
                 fit: BoxFit.cover
               )
              ),
              child: ListView(
                padding: EdgeInsets.only(bottom: 200),
                              children: <Widget>[
                                new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
                 Container(
                   margin: EdgeInsets.fromLTRB(10, 15, 10, 0),
                   width: 370,
                   height: 250,
                   decoration: new BoxDecoration(
                     borderRadius: BorderRadius.circular(20),
                     color: Colors.white.withOpacity(0.3),
                   ),
                   child: new Row(
                     mainAxisAlignment: MainAxisAlignment.center,
                     children: <Widget>[
                       new Column(
                         children: <Widget>[
                          //  Padding(
                          //    padding: const EdgeInsets.only(top: 50),
                          //    child: new Row(
                          //      children: <Widget>[
                          //        Padding(
                          //          padding: const EdgeInsets.only(right: 5),
                          //          child: new Icon(MdiIcons.instagram,color:Color(0xff0E7462) ,),
                          //        ),
                          //        Padding(
                          //          padding: const EdgeInsets.only(right: 130),
                          //          child: new Text('green.espe',textAlign: TextAlign.left,style: new TextStyle(color: Colors.white,)),
                          //        ),
                          //        new Text('اینستاگرام',style: new TextStyle(color: Color(0xff0E7462),)),
                          //      ],
                          //    ),
                          //  ),
                           Padding(
                             padding: const EdgeInsets.only(top: 50),
                             child: Container(
                               width: 300,
                               child: new Row(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.only(right: 5),
                                          child: new Icon(MdiIcons.instagram,color:Color(0xff0E7462) ,),
                                        ),
                                        new Text('green.espe',textAlign: TextAlign.left,style: new TextStyle(color: Colors.white,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ])),
                                      ],
                                    ),
                                   Container(child: new Text('اینستاگرام',textAlign: TextAlign.right,style: new TextStyle(color: Color(0xff0E7462),))),
                                 ],
                               ),
                             ),
                           ),
                           Container(
                             width: 300,
                             padding: const EdgeInsets.only(top: 7),
                             child: new Row(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                               children: <Widget>[


                                  Row(
                                    children: <Widget>[
                                                                       Padding(
                                   padding: const EdgeInsets.only(right: 5),
                                   child: new Icon(MdiIcons.email,color:Color(0xff0E7462) ,),
                                 ),
                                      new Text('espegroups@gmail.com',textAlign: TextAlign.left,style: new TextStyle(color: Colors.white,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ])),
                                    ],
                                  ),
                               
                                 Container(child: new Text('ایمیل',textAlign: TextAlign.right,style: new TextStyle(color: Color(0xff0E7462),))),
                               ],
                             ),
                           ),
                           Padding(
                             padding: const EdgeInsets.only(top: 7),
                             child: Container(
                               width: 300,
                               child: new Row(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: <Widget>[
                          

                                    Row(
                                      children: <Widget>[
                                                 Padding(
                                     padding: const EdgeInsets.only(right: 5),
                                     child: new Icon(MdiIcons.earth,color:Color(0xff0E7462) ,),
                                   ),
                                        new Text('www.espegroups.com',textAlign: TextAlign.left,style: new TextStyle(color: Colors.white,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ])),
                                      ],
                                    ),
                                   Container(child: new Text('وب سایت',textAlign: TextAlign.right,style: new TextStyle(color: Color(0xff0E7462),))),
                                 ],
                               ),
                             ),
                           ),
                           Padding(
                             padding: const EdgeInsets.only(top: 7),
                             child: Container(
                               width: 300,
                               child: new Row(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: <Widget>[

                                
                                    Row(
                                      children: <Widget>[
                                                                           Padding(
                                     padding: const EdgeInsets.only(right: 5),
                                     child: new Icon(MdiIcons.accountQuestion ,color:Color(0xff0E7462) ,),
                                   ),
                                        new Text('0912 530 1209',textAlign: TextAlign.left,style: new TextStyle(fontFamily: 'IRANSans(FaNum)',color: Colors.white,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ])),
                                      ],
                                    ),
                       
                                   Container(child: new Text('روابط عمومی',textAlign: TextAlign.right,style: new TextStyle(color: Color(0xff0E7462),))),
                                 ],
                               ),
                             ),
                           ),
                           Padding(
                             padding: const EdgeInsets.only(top: 7),
                             child: Container(
                               width: 300,
                               child: new Row(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: <Widget>[
                                
                                    Row(
                                      children: <Widget>[
                                                                           Padding(
                                     padding: const EdgeInsets.only(right: 5),
                                     child: new Icon(MdiIcons.accountClock ,color:Color(0xff0E7462) ,),
                                   ),
                                        new Text('0930 661 3683',textAlign: TextAlign.left,style: new TextStyle(fontFamily: 'IRANSans(FaNum)',color: Colors.white,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ])),
                                      ],
                                    ),
                       
                                   Container(child: new Text('پشتیبانی',textAlign: TextAlign.right,style: new TextStyle(color: Color(0xff0E7462),))),
                                 ],
                               ),
                             ),
                           ),
                          
                         ],
                       )
                     ],
                   ),
                 ),
                Container(
                  child: 
                  Padding(
                    padding: const EdgeInsets.only(top: 15),
                    child: new Text('با انتقادات و پیشنهادات خود, ما را در حفاظت از محیط زیست یاری دهید',textAlign: TextAlign.center,style: new TextStyle(color: Colors.white),),
                  ),
                  width: 300,
                ),
                     CmForm(
                      loginkey: formKey,
                      msgOnSaved:msgOnSaved
                    ),
                        GestureDetector(
                            child: Container(
                            width: 50,
                            padding: EdgeInsets.all(3),
                              decoration: new BoxDecoration(
                     borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                   ),
                            child: Center(child: new Text('ثبت',style: TextStyle(color:Color(0xff36b07a)),))
                          ),
                          onTap: (){
                          if(formKey.currentState.validate()) {
                          formKey.currentState.save();
                          sendDatatomsg();
                          formKey.currentState.reset();
                        }
                          },
                        )
            ],
          ),
                              ]), 
              ),
            
                )
              ),
        new Positioned(
          child: new Container(
            margin: EdgeInsets.only(top:MediaQuery.of(context).size.height - 155),
            child: new Scaffold(
              backgroundColor: Colors.transparent,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: new Transform.scale(
        scale: 1.2,
      child:FloatingActionButton(
        backgroundColor:Color(0xff43d1c5),
        elevation: 5,
        child:new Icon(Icons.home,size: 28,),
        onPressed: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:UserHome() ,)));
        }
      ),
      ),
      bottomNavigationBar: new BottomAppBar(
          notchMargin: 16.0,
          shape: CircularNotchedRectangle(),
          color: Colors.white,
          elevation: 20,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween, //spacebetween if we have 2 children
            children: <Widget>[
                        Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: Text('${scoreT}:امتیاز',style: new TextStyle(color:Color(0xff36b07a)),)
                ),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: new IconButton(
                    icon: Icon(Icons.menu,color:Color(0xff36b07a)),
                    color:Color(0xff36b07a),
                    iconSize: 35,
                    onPressed: (){
                      _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                ),
             ],
          ),
        )
          ),
        )
        )
        ],
    ),
    );
  }
    sendDatatomsg() async {
    final response = await http.post('http://130.185.74.192:3000/api/v1/user/getmsg',body: {"msg":_msgValue,'name':nameD});
    var responseBody = json.decode(response.body);
    print(responseBody);
      if(responseBody['success'] == true){
        _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          content: new Text(
          'پیام شما دریافت شد... سپاسگزاریم',
          style: new TextStyle(fontFamily: 'IRANSans'),
          textAlign: TextAlign.right,
        ),
        )
      );
    } else {
        _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          content: new Text(
          "خطا",
          style: new TextStyle(fontFamily: 'IRANSans'),
          textAlign: TextAlign.right,
        ),
        )
      );
    }
   
  }
  getName() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      nameD = prefs.getString('userName');
      scoreT = prefs.getString('Score');
    });
  }
}