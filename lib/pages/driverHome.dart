import 'package:flutter/material.dart';
import 'package:pushe/pushe.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:espe/pages/userInfo.dart';
import 'dart:convert';
class DriverHome extends StatefulWidget {
  // final data;
  // const DriverHome({@required this.data,Key key}):super(key:key);
  @override
  _DriverHomeState createState() => _DriverHomeState();
  
}

class _DriverHomeState extends State<DriverHome> {
  List users = new List();
  bool _userLength = true;
  String code;
  @override
  void initState() {
     Pushe.getPusheId().then((pusheId){
       code = pusheId;
     });
    // TODO: implement initState
    print(code);
    super.initState();
    users.clear();
    getUsersData();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: new Text('اِسپه'),
        backgroundColor: Color(0xffb7dc53),
      ),
      body:new RefreshIndicator(
      child: _userLength
      ?new ListView.builder(
        itemCount: 1,
        itemBuilder: (context,index){
          return Padding(
            padding: const EdgeInsets.only(top: 10),
            child: new GestureDetector(
               child: new Column(
                children: <Widget>[
                  new ListTile(
                    title: new Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Text('درخواستی وجود ندارد')
                      ],
                    ),
                  ),
                ],
              ),
             onTap: null,
             ),
          );
        },
      ) 
      :new ListView.builder(
        itemCount: users.length,
        itemBuilder: (context,index){
          return Padding(
            padding: const EdgeInsets.only(top: 10),
            child: new GestureDetector(
               child: new Column(
                children: <Widget>[
                  new ListTile(
                    title: new Row(
                      children: <Widget>[
                        new Text(users[index]['name'])
                      ],
                    ),
                    subtitle: Padding(
                      padding: const EdgeInsets.only(top:5),
                      child: new Row(
                         children: <Widget>[
                        new Text(users[index]['address']),
                         ]
                      ),
                    )
                  ),
                  Divider(
                    
                  )
                ],
              ),
             onTap: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) => new Directionality(textDirection: TextDirection.rtl,child:new UserinfoView(data:users[index]),)));
             },
             ),
          );
        },
      ), 
        onRefresh: _handleRefresh,
      )
    );
  }
  getUsersData() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    var user_id = prefs.getString("npcode");
    // prefs.clear();
    users.clear();
    //print("code:"+code);
    print("user_id:"+user_id);
    final response = await http.get('http://130.185.74.192:3000/api/v1/users');
    if(code != null){
    final response2 = await http.post('http://130.185.74.192:3000/api/v1/user/npcode',body:{"user_id":user_id,"code":code});
    var responseBody2 = json.decode(response2.body);
    print(responseBody2);
    }
    var responseBody = json.decode(response.body);

    await responseBody.forEach((item){
      if(item['activeRec'] == true){
        // print(item['name']);
        setState(() {
         users.add(item);
         _userLength =false; 
        });
        // print(users);
      }
    });
    if(users.length == 0){
      setState(() {
       _userLength =true;  
      });
    }
     print(users.length);
  }
  Future<Null> _handleRefresh() async{
    await getUsersData();
    return null;
  }
}