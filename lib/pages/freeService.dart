import 'package:espe/components/drawer.dart';
import 'package:espe/pages/userHome.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FreeServiceView extends StatefulWidget {
  @override
  _FreeServiceViewState createState() => _FreeServiceViewState();
}

class _FreeServiceViewState extends State<FreeServiceView> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String nameD = '';
  var scoreT = '0';
      @override
    @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getName();
  }
  @override
  Widget build(BuildContext context) {
    var page =MediaQuery.of(context).size;
    return new Scaffold(
    appBar: new AppBar(
      backgroundColor: Colors.white,
      title:Padding(
        padding: const EdgeInsets.only(left: 0),
        child: Center(child: Text('خدمات رایگان',style: new TextStyle(color:Color(0xff36b07a)),)),
      ),
    ),
    key: _scaffoldKey,
    endDrawer: SizedBox(child: buildDrawerLayout(context,nameD),width: 170,),
    body:new Stack(
      children: <Widget>[
          new Material(
          child: new Container(
            color: Colors.red,
            child:new Container(
              height: page.height,
              width: page.width,
              decoration: BoxDecoration(
               image: new DecorationImage(
                 image: new AssetImage("assets/images/espebg.png"),
                 fit: BoxFit.cover
               )
              ),
              child: ListView(
                padding: EdgeInsets.only(bottom: 300),
                children: <Widget>[
                  new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
                   Container(
                     margin: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                     width: 370,
                     height: 150,
                     decoration: new BoxDecoration(
                       borderRadius: BorderRadius.circular(20),
                       color: Colors.white.withOpacity(0.3),
                     ),
                     child: new Row(
                       mainAxisAlignment: MainAxisAlignment.end,
                       children: <Widget>[
                         new Column(
                           children: <Widget>[
                             Container(
                               width: 210,
                               margin:EdgeInsets.only(top: 25),
                               child: 
                               Text('سطل رایگان',
                               textAlign: TextAlign.right,
                               style: TextStyle(color: Colors.white,fontWeight: FontWeight.w700,fontSize: 17,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(1, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                               ),
                             ),
                             Container(
                               width: 210,
                               margin:EdgeInsets.only(top: 5),
                               child: 
                               Text('این سطل به قطر نیم متر و ارتفاع یک متر سطلی مناسب برای جمع آوری زباله خشک و سبک است که از جنس کارتن پلاست می باشد.',
                               textAlign: TextAlign.right,
                               textDirection: TextDirection.rtl,
                               style: TextStyle(color: Colors.white,fontWeight: FontWeight.w700,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                               ),
                             )
                           ],
                         )
                         ,
                         new Padding(
                           child: Image.asset('assets/images/ico1.png'),
                           padding: EdgeInsets.fromLTRB(20, 20, 30, 20),
                           ),
                       ],
                     ),
                   ),
                   Container(
                     width: 370,
                     height: 150,
                     margin: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                     decoration: new BoxDecoration(
                       borderRadius: BorderRadius.circular(20),
                       color: Colors.white.withOpacity(0.3),
                     ),
                     child: new Row(
                       mainAxisAlignment: MainAxisAlignment.end,
                       children: <Widget>[
                         new Column(
                           children: <Widget>[
                             Container(
                               width: 210,
                               margin:EdgeInsets.only(top: 25),
                               child: 
                               Text('کیسه رایگان',
                               textAlign: TextAlign.right,
                               style: TextStyle(color: Colors.white,fontSize: 17,fontWeight: FontWeight.w700,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(1, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                               ),
                             ),
                             Container(
                               width: 210,
                               margin:EdgeInsets.only(top: 5),
                               child: 
                               Text('ابعاد این کیسه به درخواست شما متغیر است و پس از هر بار استفاده کیسه جدید تقدیم شما خواهد شد.',
                               textAlign: TextAlign.right,
                               textDirection: TextDirection.rtl,
                               style: TextStyle(color: Colors.white,fontWeight: FontWeight.w700,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                               ),
                             )
                           ],
                         )
                         ,
                         new Padding(
                           child: Image.asset('assets/images/ico2.png'),
                           padding: EdgeInsets.fromLTRB(20, 20, 30, 20),
                           ),
                       ],
                     ),
                   ),
                   Container(
                     width: 370,
                     height: 150,
                     margin: EdgeInsets.symmetric(horizontal: 10,vertical: 5),
                     decoration: new BoxDecoration(
                       borderRadius: BorderRadius.circular(20),
                       color: Colors.white.withOpacity(0.3),
                     ),
                     child: new Row(
                       mainAxisAlignment: MainAxisAlignment.end,
                       children: <Widget>[
                         new Column(
                           children: <Widget>[
                             Container(
                               width: 210,
                               margin:EdgeInsets.only(top: 25),
                               child: 
                               Text('سطل سفارشی',
                               textAlign: TextAlign.right,
                               style: TextStyle(color: Colors.white,fontWeight: FontWeight.w700,fontSize: 17,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(1, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                               ),
                             ),
                             Container(
                               width: 210,
                               margin:EdgeInsets.only(top: 5),
                               child: 
                               Text('این سطل در دو حالت برای کاربران گرامی قابل دریافت است:',
                               textAlign: TextAlign.right,
                               textDirection: TextDirection.rtl,
                               style: TextStyle(color: Colors.white,fontWeight: FontWeight.w700,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                               ),
                             ),
                             Container(
                               width: 210,
                               margin:EdgeInsets.only(top: 5),
                               child: 
                               Text('امتیاز شما بالای 1000 باشد-',
                               textAlign: TextAlign.right,
                               style: TextStyle(color: Colors.white,fontFamily: 'IRANSans(FaNum)',fontWeight: FontWeight.w700,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                               ),
                             ),
                             Container(
                               width: 210,
                               margin:EdgeInsets.only(top: 5),
                               child: 
                               Text('توسط شما خریداری شود-',
                               textAlign: TextAlign.right,
                               style: TextStyle(color: Colors.white,fontWeight: FontWeight.w700,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                               ),
                             )
                           ],
                         )
                         ,
                         new Padding(
                           child: Image.asset('assets/images/ico3.png'),
                           padding: EdgeInsets.fromLTRB(20, 20, 30, 20),
                           ),
                       ],
                     ),
                   ),
            ],
          ),
                ],
              ), 
              ),
            
                )
              ),
        new Positioned(
          child: new Container(
            margin: EdgeInsets.only(top:MediaQuery.of(context).size.height - 155),
            child: new Scaffold(
              backgroundColor: Colors.transparent,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: new Transform.scale(
        scale: 1.2,
      child:FloatingActionButton(
        backgroundColor:Color(0xff43d1c5),
        elevation: 5,
        child:new Icon(Icons.home,size: 28,),
        onPressed: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:UserHome() ,)));
        }
      ),
      ),
      bottomNavigationBar: new BottomAppBar(
          notchMargin: 16.0,
          shape: CircularNotchedRectangle(),
          color: Colors.white,
          elevation: 20,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween, //spacebetween if we have 2 children
            children: <Widget>[
            Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: Text('${scoreT}:امتیاز',style: new TextStyle(color:Color(0xff36b07a)),)
                ),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: new IconButton(
                    icon: Icon(Icons.menu,color:Color(0xff36b07a)),
                    color:Color(0xff36b07a),
                    iconSize: 35,
                    onPressed: (){
                      _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                ),
             ],
          ),
        )
          ),
        )
        )
        ],
    ),
    );
  }
    getName() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      nameD = prefs.getString('userName');
      scoreT = prefs.getString('Score');
    });
  }
}