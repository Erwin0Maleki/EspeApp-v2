import 'package:espe/components/drawer.dart';
import 'package:espe/pages/learnV2.dart';
import 'package:espe/pages/userHome.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
class FriendsView extends StatefulWidget {
  @override
  _FriendsViewState createState() => _FriendsViewState();
}
class _FriendsViewState extends State<FriendsView> {
  String code = '...';
  var scoreT = '0';
  final _scaffoldKey = GlobalKey<ScaffoldState>();
     String nameD = '';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getName();
    getsubCode();
  }
  Widget build(BuildContext context) {
    var page =MediaQuery.of(context).size;
    return new Scaffold(
    appBar: new AppBar(
      backgroundColor: Colors.white,
      title:Padding(
        padding: const EdgeInsets.only(left: 0),
        child: Center(child: Text('معرفی به دوستان',style: new TextStyle(color:Color(0xff36b07a)),)),
      ),
      
    ),
    key: _scaffoldKey,
    endDrawer: SizedBox(child: buildDrawerLayout(context,nameD),width: 170,),
    body:new Stack(
      children: <Widget>[
          new Material(
          child: new Container(
            color: Colors.red,
            child:new Container(
              height: page.height,
              width: page.width,
              decoration: BoxDecoration(
               image: new DecorationImage(
                 image: new AssetImage("assets/images/espebg.png"),
                 fit: BoxFit.cover
               )
              ),
              child: ListView(
                padding: EdgeInsets.only(bottom: 100),
                children: <Widget>[
                  new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Container(
                width: 60,
                height: 60,
                margin: EdgeInsets.only(top: 60),
                decoration: BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/logoespe.png")
                  )
                ),
              ),
                new Padding(
                child: new Text('اِســپه',style: new TextStyle(fontSize: 16,color: Colors.white,fontWeight:FontWeight.bold,shadows: <Shadow>[
                                    Shadow(
                                     offset:Offset(0, 0),
                                     blurRadius: 3.0,
                                     color: Colors.black
                                     )
                                   ])),
                padding: new EdgeInsets.fromLTRB(5,10,0,0)
              ),
           Container(
                 margin: EdgeInsets.fromLTRB(10, 40, 10, 0),
                 width: 370,
                 height: 150,
                 decoration: new BoxDecoration(
                   borderRadius: BorderRadius.circular(20),
                   color: Colors.white.withOpacity(0.3),
                 ),
                 child:Container(
                           width: 360,
                           padding: EdgeInsets.fromLTRB(10, 30, 10, 0),
                           child:Directionality(
                            child: 
                            Column(
                              children: <Widget>[
                                Text('کد زیر را با دوستان خود به اشتراک بگذارید و پس از ارائه اولین سرویس، امتیاز دریافت کنید!',
                                 textAlign: TextAlign.center,
                                 style: TextStyle(color: Colors.white,fontSize: 15.5,shadows: <Shadow>[
                                    Shadow(
                                     offset:Offset(0, 0),
                                     blurRadius: 3.0,
                                     color: Colors.black45
                                     )
                                   ]),
                                 ),
                                 Container(
                                   width: 100,
                                   height: 40,
                                   margin: EdgeInsets.only(top: 20),
                                   alignment: Alignment.center,
                                   decoration: BoxDecoration(
                                     color: Colors.white,
                                     borderRadius: BorderRadius.circular(20),
                                   ),
                                   child: Text(code,style: TextStyle(color:Color(0xff2aac7d),fontSize: 16))
                                 )
                              ],
                            ),textDirection: TextDirection.rtl,
                           ),
                         ),
               ),
               IconButton(icon:Icon(Icons.share,color:Colors.white),iconSize: 40,onPressed: (){
                 Share.share('سلام، من برای بازیافت زباله از "اسپه" استفاده می‌کنم :http://cafebazaar.ir/app/?id=com.espe&ref=share');
               },)
            ],
          ), 
                ],
              )
              ),
            
                )
              ),
        new Positioned(
          child: new Container(
            margin: EdgeInsets.only(top:MediaQuery.of(context).size.height - 155),
            child: new Scaffold(
              backgroundColor: Colors.transparent,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: new Transform.scale(
        scale: 1.2,
      child:FloatingActionButton(
        backgroundColor:Color(0xff43d1c5),
        elevation: 5,
        child:new Icon(Icons.home,size: 28,),
        onPressed: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:UserHome() ,)));
        }
      ),
      ),
      bottomNavigationBar: new BottomAppBar(
          notchMargin: 16.0,
          shape: CircularNotchedRectangle(),
          color: Colors.white,
          elevation: 20,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween, //spacebetween if we have 2 children
            children: <Widget>[
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: Text('${scoreT}:امتیاز',style: new TextStyle(color:Color(0xff36b07a)),)
                ),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: new IconButton(
                    icon: Icon(Icons.menu,color:Color(0xff36b07a)),
                    color:Color(0xff36b07a),
                    iconSize: 35,
                    onPressed: (){
                      _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                ),
             ],
          ),
        )
          ),
        )
        )
        ],
    ),
    );
  }
    getName() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      nameD = prefs.getString('userName');
      scoreT = prefs.getString('Score');
    });
  }
    getsubCode() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('userToken');
    final response = await http.get('http://130.185.74.192:3000/api/v1/user/${apiToken}');
    var responseBody = json.decode(response.body);
    setState(() {
     code = responseBody['data']['userSubCode'].toString(); 
    });
    } 
}