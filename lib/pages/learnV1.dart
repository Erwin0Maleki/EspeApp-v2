import 'package:espe/components/drawer.dart';
import 'package:espe/pages/learnV2.dart';
import 'package:espe/pages/userHome.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LearnV1 extends StatefulWidget {
  @override
  _LearnV1State createState() => _LearnV1State();
}

class _LearnV1State extends State<LearnV1> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String nameD = '';
  var scoreT = '0';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getName();
  }
  @override
  Widget build(BuildContext context) {
    var page =MediaQuery.of(context).size;
    return new Scaffold(
    appBar: new AppBar(
      backgroundColor: Colors.white,
      title:Padding(
        padding: const EdgeInsets.only(left: 0),
        child: Center(child: Text('آموزش',style: new TextStyle(color:Color(0xff36b07a)),)),
      ),
      leading:GestureDetector(
        child: Icon(Icons.arrow_back,color:Color(0xff36b07a) ,),
        onTap: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:LearnV2() ,)));
        },
      )
      
    ),
    key: _scaffoldKey,
    endDrawer: SizedBox(child: buildDrawerLayout(context,nameD),width: 170,),
    body:new Stack(
      children: <Widget>[
          new Material(
          child: new Container(
            color: Colors.red,
            child:new Container(
              height: page.height,
              width: page.width,
              decoration: BoxDecoration(
               image: new DecorationImage(
                 image: new AssetImage("assets/images/espebg.png"),
                 fit: BoxFit.cover
               )
              ),
              child: ListView(
                padding: EdgeInsets.only(bottom: 100),
                children: <Widget>[
            new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
                Container(
                 margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                 height: 110,
                 decoration: new BoxDecoration(
                   borderRadius: BorderRadius.circular(20),
                   color: Colors.white.withOpacity(0.3),
                 ),
                 child: new Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                       new Padding(
                       child: Image.asset('assets/images/repaper.png',width: 140,),
                       padding: EdgeInsets.only(left: 15)
                       ),
                     new Row(
                       children: <Widget>[
                         Container(
                           padding: EdgeInsets.only(right: 5),
                           child:Text('کاغذ و دفتر و کتاب',
                           textAlign: TextAlign.right,
                           style: TextStyle(color: Colors.white,fontSize: 16,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                           ),
                         ),
                         Container(
                           child: Icon(Icons.check,color: Colors.white,),
                           padding: EdgeInsets.only(right: 15),
                         )
                       ],
                     )
                     ,
                   ],
                 ),
               ),
                Container(
                 margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                 height: 110,
                 decoration: new BoxDecoration(
                   borderRadius: BorderRadius.circular(20),
                   color: Colors.white.withOpacity(0.3),
                 ),
                 child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                       new Padding(
                       child: Image.asset('assets/images/remoghava.png',width: 140,),
                       padding: EdgeInsets.only(left: 15)
                       ),
                     new Row(
                       children: <Widget>[
                         Container(
                           padding: EdgeInsets.only(right: 5),
                           child:Text('مقوا و کارتن',
                           textAlign: TextAlign.right,
                           style: TextStyle(color: Colors.white,fontSize: 16,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                           ),
                         ),
                         Container(
                           child: Icon(Icons.check,color: Colors.white,),
                           padding: EdgeInsets.only(right: 15),
                         )
                       ],
                     )
                     ,
                   ],
                 ),
               ),
                 Container(
                 margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                 height: 110,
                 decoration: new BoxDecoration(
                   borderRadius: BorderRadius.circular(20),
                   color: Colors.white.withOpacity(0.3),
                 ),
                 child: new Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                       new Padding(
                       child: Image.asset('assets/images/remetal.png',width: 120,),
                       padding: EdgeInsets.only(left: 10)
                       ),
                     new Row(
                       children: <Widget>[
                         Container(
                           width: 170,
                           padding: EdgeInsets.only(right: 5),
                           child:Text('انواع فلزات',
                           textAlign: TextAlign.right,
                           style: TextStyle(color: Colors.white,fontSize: 16,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                           ),
                         ),
                         Container(
                           child: Icon(Icons.check,color: Colors.white,),
                           padding: EdgeInsets.only(right: 15),
                         )
                       ],
                     )
                     ,
                   ],
                 ),
               ),
                 Container(
                 margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                 height: 110,
                 decoration: new BoxDecoration(
                   borderRadius: BorderRadius.circular(20),
                   color: Colors.white.withOpacity(0.3),
                 ),
                 child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                       new Padding(
                       child: Image.asset('assets/images/rehardplastic.png',width: 120,),
                       padding: EdgeInsets.only(left: 10)
                       ),
                     new Row(
                       children: <Widget>[
                         Container(
                           width: 170,
                           padding: EdgeInsets.only(right: 5),
                           child:Text('پلاستیک سخت',
                           textAlign: TextAlign.right,
                           style: TextStyle(color: Colors.white,fontSize: 16,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                           ),
                         ),
                         Container(
                           child: Icon(Icons.check,color: Colors.white,),
                           padding: EdgeInsets.only(right: 15),
                         )
                       ],
                     )
                     ,
                   ],
                 ),
               ),
                 Container(
                 margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                 height: 110,
                 decoration: new BoxDecoration(
                   borderRadius: BorderRadius.circular(20),
                   color: Colors.white.withOpacity(0.3),
                 ),
                 child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                       new Padding(
                       child: Image.asset('assets/images/reglass.png',width: 120,),
                       padding: EdgeInsets.only(left: 10)
                       ),
                     new Row(
                       children: <Widget>[
                         Container(
                           width: 170,
                           padding: EdgeInsets.only(right: 5),
                           child:Text('ظروف شیشه ای و کارتن نوشیدنی',
                           textAlign: TextAlign.right,
                           style: TextStyle(color: Colors.white,fontSize: 16,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                           ),
                         ),
                         Container(
                           child: Icon(Icons.check,color: Colors.white,),
                           padding: EdgeInsets.only(right: 15),
                         )
                       ],
                     )
                     ,
                   ],
                 ),
               ),
            ],
          ), 
                ],
              )
              ),
            
                )
              ),
        new Positioned(
          child: new Container(
            margin: EdgeInsets.only(top:MediaQuery.of(context).size.height - 155),
            child: new Scaffold(
              backgroundColor: Colors.transparent,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: new Transform.scale(
        scale: 1.2,
      child:FloatingActionButton(
        backgroundColor:Color(0xff43d1c5),
        elevation: 5,
        child:new Icon(Icons.home,size: 28,),
        onPressed: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:UserHome() ,)));
        }
      ),
      ),
      bottomNavigationBar: new BottomAppBar(
          notchMargin: 16.0,
          shape: CircularNotchedRectangle(),
          color: Colors.white,
          elevation: 20,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween, //spacebetween if we have 2 children
            children: <Widget>[
                       Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: Text('${scoreT}:امتیاز',style: new TextStyle(color:Color(0xff36b07a)),)
                ),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: new IconButton(
                    icon: Icon(Icons.menu,color:Color(0xff36b07a)),
                    color:Color(0xff36b07a),
                    iconSize: 35,
                    onPressed: (){
                      _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                ),
             ],
          ),
        )
          ),
        )
        )
        ],
    ),
    );
  }
    getName() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      nameD = prefs.getString('userName');
      scoreT = prefs.getString('Score');
    });
  }
}