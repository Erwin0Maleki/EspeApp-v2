import 'package:espe/components/drawer.dart';
import 'package:espe/pages/learnV1.dart';
import 'package:espe/pages/userHome.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LearnV2 extends StatefulWidget {
  @override
  _LearnV2State createState() => _LearnV2State();
}

class _LearnV2State extends State<LearnV2> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
   String nameD = '';
   var scoreT = '0';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getName();
  }
  @override
  Widget build(BuildContext context) {
    var page =MediaQuery.of(context).size;
    return new Scaffold(
    appBar: new AppBar(
      backgroundColor: Colors.white,
      title:Padding(
        padding: const EdgeInsets.only(left: 0),
        child: Center(child: Text('آموزش',style: new TextStyle(color:Color(0xff36b07a)),)),
      ),
      actions: <Widget>[
        GestureDetector(
        child: Padding(
          padding: const EdgeInsets.only(right: 15),
          child: Icon(Icons.arrow_forward,color:Color(0xff36b07a) ,),
        ),
         onTap: (){
         Navigator.pushReplacement(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:LearnV1() ,)));
        },
      )
      ],
      
    ),
    key: _scaffoldKey,
    endDrawer: SizedBox(child: buildDrawerLayout(context,nameD),width: 170,),
    body:new Stack(
      children: <Widget>[
          new Material(
          child: new Container(
            color: Colors.red,
            child:new Container(
              height: page.height,
              width: page.width,
              decoration: BoxDecoration(
               image: new DecorationImage(
                 image: new AssetImage("assets/images/espebg.png"),
                 fit: BoxFit.cover
               )
              ),
              child: ListView(
                padding: EdgeInsets.only(bottom: 100),
                children: <Widget>[
                  new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
           Container(
                 margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                 height: 110,
                 decoration: new BoxDecoration(
                   borderRadius: BorderRadius.circular(20),
                   color: Colors.white.withOpacity(0.3),
                 ),
                 child: new Row(
                     mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                       new Container(
                        width: 150,

                        decoration: BoxDecoration(
                          
                          color: Color(0xffEE333D),
                          borderRadius: BorderRadius.circular(20)
                        ),
                       child: Image.asset('assets/images/red1.png',width: 90,),
                       ),
                     new Row(
                       children: <Widget>[
                        Text('دستمال کاغذی',
                           textAlign: TextAlign.right,
                           style: TextStyle(color: Colors.white,fontSize: 16,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                           ),
                         Container(
                           child: Icon(Icons.clear,color: Colors.red,),
                           padding: EdgeInsets.only(right: 10),
                         )
                       ],
                     )
                     ,
                   ],
                 ),
               ),
           Container(
                 margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                 height: 110,
                 decoration: new BoxDecoration(
                   borderRadius: BorderRadius.circular(20),
                   color: Colors.white.withOpacity(0.3),
                 ),
                 child: new Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                       new Container(
                        
                        width: 150,
                        decoration: BoxDecoration(
                          
                          color: Color(0xffEE333D),
                          borderRadius: BorderRadius.circular(20)
                        ),
                       child: Image.asset('assets/images/red2.png',width: 100,),
                       ),
                     new Row(
                       children: <Widget>[
                        Text('کیسه پلاستیکی',
                           textAlign: TextAlign.right,
                           style: TextStyle(color: Colors.white,fontSize: 16,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                           ),
                         Container(
                           child: Icon(Icons.clear,color: Colors.red,),
                           padding: EdgeInsets.only(right: 15),
                         )
                       ],
                     )
                     ,
                   ],
                 ),
               ),
               Container(
                 margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                 height: 110,
                 decoration: new BoxDecoration(
                   borderRadius: BorderRadius.circular(20),
                   color: Colors.white.withOpacity(0.3),
                 ),
                 child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                       new Container(
                        
                        width: 150,

                        decoration: BoxDecoration(
                          
                          color: Color(0xffEE333D),
                          borderRadius: BorderRadius.circular(20)
                        ),
                       child: Image.asset('assets/images/red3.png',width: 100,),
                       ),
                     new Row(
                       children: <Widget>[
                        Text('پسماند عفونی',
                           textAlign: TextAlign.right,
                           style: TextStyle(color: Colors.white,fontSize: 16,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                           ),
                         Container(
                           child: Icon(Icons.clear,color: Colors.red,),
                           padding: EdgeInsets.only(right: 15),
                         )
                       ],
                     )
                     ,
                   ],
                 ),
               ),
               Container(
                 margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                 height: 110,
                 decoration: new BoxDecoration(
                   borderRadius: BorderRadius.circular(20),
                   color: Colors.white.withOpacity(0.3),
                 ),
                 child: new Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                       new Container(
                        
                        width: 150,

                        decoration: BoxDecoration(
                          
                          color: Color(0xffEE333D),
                          borderRadius: BorderRadius.circular(20)
                        ),
                       child: Image.asset('assets/images/red4.png',width: 100,),
                       ),
                     new Row(
                       children: <Widget>[
                        Text('پلاستیک فوم',
                           textAlign: TextAlign.right,
                           style: TextStyle(color: Colors.white,fontSize: 16,shadows: <Shadow>[
                                Shadow(
                                 offset:Offset(0, 0),
                                 blurRadius: 3.0,
                                 color: Colors.black45
                                 )
                               ]),
                           ),
                         Container(
                           child: Icon(Icons.clear,color: Colors.red,),
                           padding: EdgeInsets.only(right: 15),
                         )
                       ],
                     )
                     ,
                   ],
                 ),
               ),
                 Container(
                 margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                 height: 110,
                 decoration: new BoxDecoration(
                   borderRadius: BorderRadius.circular(20),
                   color: Colors.white.withOpacity(0.3),
                 ),
                 child: new Row(
                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                   children: <Widget>[
                       new Container(
                        
                        width: 150,

                        decoration: BoxDecoration(
                          
                          color: Color(0xffEE333D),
                          borderRadius: BorderRadius.circular(20)
                        ),
                       child: Image.asset('assets/images/red5.png',width: 100),
                       ),
                     new Row(
                       children: <Widget>[
                        Container(
                          width: 150,
                          child: Text('بسته بندی چیپس و شکلات',
                             textAlign: TextAlign.right,
                             style: TextStyle(color: Colors.white,fontSize: 16,shadows: <Shadow>[
                                  Shadow(
                                   offset:Offset(0, 0),
                                   blurRadius: 3.0,
                                   color: Colors.black45
                                   )
                                 ]),
                             ),
                        ),
                         Container(
                           child: Icon(Icons.clear,color: Colors.red,),
                           padding: EdgeInsets.only(right: 15),
                         )
                       ],
                     )
                     ,
                   ],
                 ),
               ),
            ],
          ), 
                ],
              )
              ),
            
                )
              ),
        new Positioned(
          child: new Container(
            margin: EdgeInsets.only(top:MediaQuery.of(context).size.height - 155),
            child: new Scaffold(
              backgroundColor: Colors.transparent,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: new Transform.scale(
        scale: 1.2,
      child:FloatingActionButton(
        backgroundColor:Color(0xff43d1c5),
        elevation: 5,
        child:new Icon(Icons.home,size: 28,),
        onPressed: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:UserHome() ,)));
        }
      ),
      ),
      bottomNavigationBar: new BottomAppBar(
          notchMargin: 16.0,
          shape: CircularNotchedRectangle(),
          color: Colors.white,
          elevation: 20,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,//spacebetween if we have 2 children
            children: <Widget>[
                         Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: Text('${scoreT}:امتیاز',style: new TextStyle(color:Color(0xff36b07a)),)
                ),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: new IconButton(
                    icon: Icon(Icons.menu,color:Color(0xff36b07a)),
                    color:Color(0xff36b07a),
                    iconSize: 35,
                    onPressed: (){
                      _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                ),
             ],
          ),
        )
          ),
        )
        )
        ],
    ),
    );
  }
    getName() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      nameD = prefs.getString('userName');
      scoreT = prefs.getString('Score');
    });
  }
}