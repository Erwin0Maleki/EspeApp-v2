import 'package:flutter/material.dart';
import 'package:espe/components/loginform.dart';
import 'package:espe/pages/userHome.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:espe/services/auth_services.dart';
import 'dart:convert';
import 'package:espe/model/token.dart';
class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  final formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  String _singText = 'ورود';
  var Token;
  var Username;
  var Password;
  String _phoneValue;
  String _passwordValue;

  phoneOnSaved(String value){
    print(value);
    _phoneValue =value;
  }

  passwordOnSaved(String value){
    print(value);
    _passwordValue = value;
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var page =MediaQuery.of(context).size;
    return new Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      body: new Container(
        child: new Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            new Container(
              height: page.height,
              width: page.width,
              decoration: BoxDecoration(
               image: new DecorationImage(
                 image: new AssetImage("assets/images/espebg.png"),
                 fit: BoxFit.cover
               )
              ),
            ),
            new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
                new Container(
                width: 98,
                height: 98,
                decoration: BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/logo.png")
                  )
                ),
              ),
               new Padding(
                child: new Text('اِسپه',style: new TextStyle(fontSize: 20,color: Colors.white,fontWeight:FontWeight.bold)),
                padding: new EdgeInsets.fromLTRB(5,10,0,0)
              ),
               new Padding(
                child:LoginForm(
                  loginkey : formKey,
                  phoneOnSaved:phoneOnSaved,
                  passwordOnSaved:passwordOnSaved
                  ),
                padding: new EdgeInsets.fromLTRB(5,30,0,0)
              ),
               new Padding(
                child:new GestureDetector(
                    onTap:() {
                      if(formKey.currentState.validate()) {
                        formKey.currentState.save();
                        sendDataforLogin();
                      }
                      },
                  child:new Container(
                  width: 320,
                  height: 45,
                  alignment: Alignment.center,
                  child: new Text(_singText,style: new TextStyle(color:new Color(0xff41b476),fontWeight:FontWeight.w700,letterSpacing: .3)),
                  decoration: new BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(25)),
                      )
                    ),
                padding: new EdgeInsets.fromLTRB(5,40,0,0)
              ),
               new Padding(
                child:new FlatButton(
                 onPressed: () {Navigator.pushNamed(context, 'register');},
                  child: new Text('اگر حساب کاربری ندارید: ثبت نام کنید',style: new TextStyle(color:new Color(0xffffffff),fontWeight:FontWeight.w300,letterSpacing: .3),),
                ),
                padding: new EdgeInsets.fromLTRB(5,5,0,0)
              ),
           
            ],
          ),
            new Container(
              margin: const EdgeInsets.only(bottom: 5),
              alignment: Alignment.bottomCenter,
              child:new Text('ESPE©',style: new TextStyle(color: new Color(0xff88caae),fontSize: 20))
              ) 
          ],
        ),
      ),
    );
  }
  sendDataforLogin() async {
  await sendDataToServer();
  }

  sendDataToServer() async{
    setState(() {
     _singText = 'صبر کنید...' ;
    });
    Map responseBody = await (new AuthServices()).sendDataForLogin({"phone":_phoneValue,"password":_passwordValue});
    // print(responseBody);
    if(responseBody['success'] == true){
      await storeData(responseBody);
      Token = responseBody['token'];
      Username = responseBody['data']['name'];
      Password = responseBody['data']['password'];
      if(responseBody['data']['driver'] == true){
           Navigator.pushReplacementNamed(context, 'driver');
         }else{
          Navigator.pushReplacement(
         context,
         new MaterialPageRoute(builder: (context) => new UserHome(data:responseBody['data'])));
         //new MaterialPageRoute(builder: (context) => new UserHome(data:responseBody['data'])));
         }
    } else {
      setState(() {
     _singText = 'ورود' ;
      });
      _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          content: new Text(
          'اطلاعات وارد شده صحیح نیست',
          style: new TextStyle(fontFamily: 'IRANSans'),
        ),
        )
      );
    }
  }

  storeData(Map userData) async{
    print(userData['token']);
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString('userToken',userData['token']);
    await prefs.setString('userName',userData['data']['name']);
    await prefs.setString('phone',userData['data']['phone']);
    await prefs.setString('password',userData['data']['password']);
    if(userData['data']['npcode'] != null || userData['data']['npcode'] != ''){
    await prefs.setString('npcode',userData['data']['_id']);
    }

  }
}