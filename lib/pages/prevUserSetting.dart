import 'dart:convert';
import 'package:espe/pages/userSetting.dart';
import 'package:http/http.dart' as http;
import 'package:espe/components/drawer.dart';
import 'package:espe/components/settingForm.dart';
import 'package:espe/pages/learnV2.dart';
import 'package:espe/pages/userHome.dart';
import 'package:flutter/material.dart';
import 'package:espe/components/registerform.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PrevUserSetting extends StatefulWidget {
  @override
  _PrevUserSettingState createState() => _PrevUserSettingState();
}

class _PrevUserSettingState extends State<PrevUserSetting> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final formRegKey = GlobalKey<FormState>();
  var userName;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    transferValue();
  }
  @override
  Widget build(BuildContext context) {
    var page =MediaQuery.of(context).size;
    return new Scaffold(
    appBar: new AppBar(
      backgroundColor: Colors.white,
      title:Padding(
        padding: const EdgeInsets.only(left: 0),
        child: Center(child: Text('در حال دریافت اطلاعات',style: new TextStyle(color:Color(0xff36b07a)),)),
      ),
      
    ),
    key: _scaffoldKey,
    endDrawer: SizedBox(child: buildDrawerLayout(context),width: 170,),
    body:new Stack(
      children: <Widget>[
          new Material(
          child:Container(
            width: page.width,
            height: page.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation(Colors.greenAccent)),
              ],
            ),
          )
              ),
        new Positioned(
          child: new Container(
            margin: EdgeInsets.only(top:MediaQuery.of(context).size.height - 155),
            child: new Scaffold(
              backgroundColor: Colors.transparent,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor:Color(0xff43d1c5),
        elevation: 5,
        child:new Icon(Icons.home),
        onPressed: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:UserHome() ,)));
        }
      ),
      bottomNavigationBar: new BottomAppBar(
          notchMargin: 10.0,
          shape: CircularNotchedRectangle(),
          color: Colors.white,
          elevation: 20,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.end, //spacebetween if we have 2 children
            children: <Widget>[
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: new IconButton(
                    icon: Icon(Icons.menu,color:Color(0xff36b07a)),
                    color:Color(0xff36b07a),
                    iconSize: 35,
                    onPressed: (){
                      _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                ),
             ],
          ),
        )
          ),
        )
        )
        ],
    ),
    );
  }
   getUserActiveState() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('userToken');
    final response = await http.get('http://130.185.74.192:3000/api/v1/user/${apiToken}');
    print(apiToken);
    var responseBody = json.decode(response.body);
    if(!(responseBody['data'] == '' || responseBody['data'] == null)){
      return responseBody['data'];
    } 
  }
   transferValue() async{
    var udata = await getUserActiveState();
         Navigator.pushReplacement(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:UserSettingView(data:udata) ,)));

  }
}