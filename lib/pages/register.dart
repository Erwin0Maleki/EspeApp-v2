import 'package:flutter/material.dart';
import 'package:espe/components/registerform.dart';
import 'package:http/http.dart' as http;
import 'package:espe/pages/userHome.dart';
import 'dart:convert';
import 'dart:async';
class RegisterView extends StatefulWidget {
  @override
  _RegisterViewState createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
    final formRegKey = GlobalKey<FormState>();
    final _scaffoldKey = GlobalKey<ScaffoldState>();
    String _regText = 'عضویت';
    String _phoneValue;
    String _passwordValue;
    String _nameValue;
    String _subCodeValue;
    String _addressValue;

    phoneOnSaved(String value){
    print(value);
    _phoneValue = value;
    }
    subCodeOnSaved(String value){
    print(value);
    _subCodeValue = value;
    }
    passwordOnSaved(String value){
    print(value);
    _passwordValue = value;
    }

    _nameOnSaved(String value){
    print(value);
    _nameValue = value;
    }

    _addressOnSaved(String value){
    print(value);
    _addressValue = value;
    }

  @override
  Widget build(BuildContext context) {
     var page =MediaQuery.of(context).size;
    return new Scaffold(
      key: _scaffoldKey,
      body: new Container(
        child: new Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            new Container(
              height: page.height,
              width: page.width,
              decoration: BoxDecoration(
               image: new DecorationImage(
                 image: new AssetImage("assets/images/espebg.png"),
                 fit: BoxFit.cover
               )
              ),
            ),
            new Padding(
            padding: const EdgeInsets.only(bottom: 30),
            child:SingleChildScrollView(
            child:new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
                new Container(
                width: 98,
                height: 98,
                decoration: BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/logo.png")
                  )
                ),
              ),
               new Padding(
                child: new Text('اِسپه',style: new TextStyle(fontSize: 20,color: Colors.white,fontWeight:FontWeight.bold)),
                padding: new EdgeInsets.fromLTRB(5,10,0,0)
              ),
               new Padding(
                child:RegisterForm(
                  registerkey : formRegKey,
                  phoneOnSaved:phoneOnSaved,
                  passswordOnSaved:passwordOnSaved,
                  nameOnSaved:_nameOnSaved,
                  addressOnSaved:_addressOnSaved,
                  subCodeOnSaved:subCodeOnSaved
                  ),
                padding: new EdgeInsets.fromLTRB(5,30,0,0)
              ),
               new Padding(
                child:new GestureDetector(
                    onTap:() async {
                      if(formRegKey.currentState.validate()){
                        formRegKey.currentState.save();
                        sendDataforRegister();
                      }
                      },
                  child:new Container(
                  width: 320,
                  height: 45,
                  alignment: Alignment.center,
                  child: new Text(_regText,style: new TextStyle(color:new Color(0xff41b476),fontWeight:FontWeight.w700,letterSpacing: .3)),
                  decoration: new BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(25)),
                      )
                    ),
                padding: new EdgeInsets.fromLTRB(5,40,0,0)
              ),
               new Padding(
                child:new FlatButton(
                 onPressed: () { Navigator.pushNamed(context, 'login');},
                  child: new Text('اگر حساب کاربری دارید: وارد شوید',style: new TextStyle(color:new Color(0xffffffff),fontWeight:FontWeight.w300,letterSpacing: .3),),
                ),
                padding: new EdgeInsets.fromLTRB(5,5,0,0)
              ),
           
            ],
           ),
          )),
            new Container(
              margin: const EdgeInsets.only(bottom: 5),
              alignment: Alignment.bottomCenter,
              child:new Text('ESPE©',style: new TextStyle(color: new Color(0xff88caae),fontSize: 20))
              ) 
          ],
        ),
      ),
    ); 
  }
  sendDataforRegister() async {
  await sendDataToServer();
  }
  sendDataToServer() async {
    setState(() {
     _regText = 'صبر کنید...' ;
    });
    print(_subCodeValue);
    if(_subCodeValue == null || _subCodeValue == ''){
      _subCodeValue= '0';
    }
    print(_subCodeValue);
    final response = await http.post('http://130.185.74.192:3000/api/v1/register',body: {"phone":_phoneValue,"password":_passwordValue,"name":_nameValue,"address":_addressValue,"subcode":_subCodeValue});
    var responseBody = json.decode(response.body);
    print(responseBody);
      if(responseBody['success'] == true){
        _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          content: new Text(
          responseBody['message'],
          style: new TextStyle(fontFamily: 'IRANSans'),
        ),
        )
      );
        Timer time = new Timer(new Duration(seconds: 2), (){
            Navigator.pushNamed(context, 'login');
        });
    } else {
       setState(() {
     _regText = 'عضویت' ;
        });
        _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          content: new Text(
          responseBody['message'],
          style: new TextStyle(fontFamily: 'IRANSans'),
        ),
        )
      );
    }
   
  }
}