import 'package:flutter/material.dart';
import 'package:pushe/pushe.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:espe/pages/userHome.dart';
import 'package:espe/pages/driverHome.dart';
import 'package:connectivity/connectivity.dart';
class SplashView extends StatefulWidget {
  @override
  SplashViewState createState() => SplashViewState();
}

class SplashViewState extends State<SplashView> with TickerProviderStateMixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  AnimationController controller;
  Animation<EdgeInsets> edgeinsetAnimation;
  Animation<double> opacityAnimation;
  Animation<Color> gradiantAnimation;
  Animation<Color> gradiantAnimation2;
    @override
  void initState() {
    super.initState();
    //Pushe.initialize();
    controller = new AnimationController(vsync: this,duration: const Duration(milliseconds: 2000));
    edgeinsetAnimation = EdgeInsetsTween(begin: EdgeInsets.only(bottom: 350),
    end: EdgeInsets.only(bottom: 0)).animate(
    new CurvedAnimation(parent: controller,curve:new Interval(0.0, 1,curve:Curves.ease)));

    opacityAnimation = Tween(begin: 0.0,
    end: 1.0).animate(
    new CurvedAnimation(parent: controller,curve:new Interval(0.0,1,curve:Curves.ease)));

    gradiantAnimation = ColorTween(begin: Color(0xff2aac7d),
    end: Color(0xffb7dc53)).animate(
    new CurvedAnimation(parent: controller,curve:new Interval(0.0,1,curve:Curves.linear)));

    gradiantAnimation2 = ColorTween(begin: Color(0xffb7dc53),
    end: Color(0xff2aac7d)).animate(
    new CurvedAnimation(parent: controller,curve:new Interval(0.0,1,curve:Curves.linear)));
    Timer time = new Timer(new Duration(seconds:1), (){
    controller.forward();
    });
    Timer time2 = new Timer(new Duration(seconds: 3), (){
    checklogin();
    });
   
    
  }

  Widget _animationBuilder(BuildContext context,Widget child){
    return new Padding(
      child: new Opacity(
        child: new Container(
                width: 98,
                height: 98,
                decoration: BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/logo.png")
                  )
                ),
              ),
              opacity: opacityAnimation.value,
    ),padding: edgeinsetAnimation.value,
            )  ;
  }
  Widget _animationBuilder2(BuildContext context,Widget child){
    return new Container(
            decoration: BoxDecoration(
            gradient: LinearGradient(
              begin:FractionalOffset.topCenter,
              end:FractionalOffset.bottomCenter ,
              colors: [gradiantAnimation.value,gradiantAnimation2.value],
              tileMode: TileMode.clamp
              )
            ),
          );    
  }
  Widget _animationBuilder3(BuildContext context,Widget child){
    return  new Opacity(
      child:new Padding(
                child: new Text('اِسپه',style: new TextStyle(fontSize: 20,color: Colors.white,fontWeight:FontWeight.bold)),
                padding: new EdgeInsets.fromLTRB(5,10,0,0)
              ) ,
              opacity: opacityAnimation.value,
    );
  }
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomPadding: false,
      body:new Stack(
        fit: StackFit.expand,
        children: <Widget>[
          //  new AnimatedBuilder(
          //       animation: controller,
          //       builder: _animationBuilder2,
          //     ),
              new Container(
              height: MediaQuery.of(context).size.height,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
               image: new DecorationImage(
                 image: new AssetImage("assets/images/espebg.png"),
                 fit: BoxFit.cover
               )
              ),
            ),
          new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new AnimatedBuilder(
                animation: controller,
                builder: _animationBuilder,
              ),
              new AnimatedBuilder(
                animation: controller,
                builder: _animationBuilder3,
              ),
             
            ],
          )
        ],
      )
    );
  }
  checklogin() async{
    
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('userToken');
     String username = prefs.getString('userName');
    String phone = prefs.getString('phone');
    String password = prefs.getString('password');
    //prefs.clear();
    // if(await checkConnectionInternet()){
    //  if(!(apiToken == null || (phone == null&&password == null))){
    //   final UPresponse = await http.post('http://130.185.74.192:3000/api/v1/login',body:{"phone":phone,"password":password});
    //   var UPresponseBody = json.decode(UPresponse.body);
    //   final response = await http.get('http://130.185.74.192:3000/api/v1/user/${apiToken}');
    //   var responseBody = json.decode(response.body);
    //   if(!(responseBody['data'] == '')){
    //      print('s1');
    //    if(responseBody['success'] == false){
    //       print('s2');
    //        if(UPresponseBody['success'] == true){
    //           print('s3');
    //         if(UPresponseBody['data']['driver'] == true){
    //            print('s4');
    //             Navigator.pushReplacementNamed(context, 'driver');
    //           }else{
    //             Navigator.pushReplacement(
    //           context,
    //           new MaterialPageRoute(builder: (context) => new UserHome(data:UPresponseBody['data'])));
    //           }
    //       }else{
    //         return Navigator.pushReplacementNamed(context, 'login');
    //       }
    //      //Navigator.pushReplacementNamed(context, 'login');
    //    }else{
    //      if(responseBody['data']['driver'] == true){
    //        print(responseBody['data']['driver']);
    //        Navigator.pushReplacementNamed(context, 'driver');
    //        print("asdasd");
    //      }else{
    //        print(responseBody['data']['driver']);
    //       Navigator.pushReplacement(
    //      context,
    //      new MaterialPageRoute(builder: (context) => new UserHome(data:responseBody['data'])));
    //      //new MaterialPageRoute(builder: (context) => new UserHome(data:responseBody['data'])));
    //      }
    //    }
    // } else {
    //              if(UPresponseBody['success'] == true){
    //         if(UPresponseBody['data']['driver'] == true){
    //             Navigator.pushReplacementNamed(context, 'driver');
    //           }else{
    //             Navigator.pushReplacement(
    //           context,
    //           new MaterialPageRoute(builder: (context) => new UserHome(data:UPresponseBody['data'])));
    //           }
    //       }else{
    //         return Navigator.pushReplacementNamed(context, 'login');
    //       }
    // }
    // }else {
    //          return Navigator.pushReplacementNamed(context, 'login');
    // }
 if(await checkConnectionInternet()){
  //prefs.clear();
  Pushe.initialize();
  if(!(apiToken == null)){
    final response = await http.get('http://130.185.74.192:3000/api/v1/user/${apiToken}');
    var responseBody = json.decode(response.body);
    print('getting data');
    if(!(responseBody['data'] == '')){
      print('checked data its not empty');
      //print(responseBody['data']);
      if(responseBody['success'] == true){
        print('success is true');
        if(responseBody['data']['driver'] == true){
          Navigator.pushReplacementNamed(context, 'driver');
        }else if(responseBody['data']['driver'] == false){
          Navigator.pushReplacement(
          context,
          new MaterialPageRoute(builder: (context) => new UserHome(data:responseBody['data'])));
        }
      }else if(responseBody['success'] == false){
        print('fail cause: the code');
        Navigator.pushReplacementNamed(context, 'login');
      }
         //Navigator.pushReplacementNamed(context, 'login');
    }else if(responseBody['data'] == ''){
        //  if(responseBody['data']['driver'] == true){
        //    print(responseBody['data']['driver']);
        //    Navigator.pushReplacementNamed(context, 'driver');
        //    print("asdasd");
        //  }else{
        //    print(responseBody['data']['driver']);
        //   Navigator.pushReplacement(
        //  context,
        //  new MaterialPageRoute(builder: (context) => new UserHome(data:responseBody['data'])));
        //  //new MaterialPageRoute(builder: (context) => new UserHome(data:responseBody['data'])));
        //  }
        Navigator.pushReplacementNamed(context, 'login');
       }
    } else if(apiToken == null) {
          //        if(UPresponseBody['success'] == true){
          //   if(UPresponseBody['data']['driver'] == true){
          //       Navigator.pushReplacementNamed(context, 'driver');
          //     }else{
          //       Navigator.pushReplacement(
          //     context,
          //     new MaterialPageRoute(builder: (context) => new UserHome(data:UPresponseBody['data'])));
          //     }
          // }else{
          //   return Navigator.pushReplacementNamed(context, 'login');
          // }
          Navigator.pushReplacementNamed(context, 'login');
    }
    }else {
     _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          duration: new Duration(hours: 2),
          content:new GestureDetector(
            onTap: (){
              _scaffoldKey.currentState.hideCurrentSnackBar();
              checklogin();
            },
            child:new Text('از اتصال دستگاه به اینترنت مطمئن شوید(برای امتحان دوباره لمس کنید)',style: new TextStyle(fontFamily: 'IRANSans'))
            ),
          )
        );
    }
  }
  Future<bool> checkConnectionInternet() async{
    var connectivityRes = await (new Connectivity().checkConnectivity());
    return connectivityRes == ConnectivityResult.mobile || connectivityRes == ConnectivityResult.wifi;
  }
}