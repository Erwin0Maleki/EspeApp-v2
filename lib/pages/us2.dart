import 'package:flutter/material.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
class UserHome extends StatefulWidget {
  final data;
  const UserHome({@required this.data,Key key}):super(key:key);
  @override
  _UserHomeState createState() => _UserHomeState();
}

class _UserHomeState extends State<UserHome> with TickerProviderStateMixin {
   bool _currentActive = true;
    var _currentbutton = 'صبر کنید';
   double _currentState = 0.0;
   AnimationController controller;
   Animation<double> rotateAnimation;
   
   @override
  void initState() {
    super.initState();
    controller = new AnimationController(vsync: this,duration: const Duration(milliseconds: 4000));
    rotateAnimation = Tween(begin: 0.0,
    end: 6.3).animate(
    new CurvedAnimation(parent: controller,curve:new Interval(0.0,1,curve:Curves.elasticInOut)));

     Timer time2 = new Timer(new Duration(seconds: 2), (){
     getUserActiveState();
    });
  }
 Widget _animationBuilder(BuildContext context,Widget child){
    return Transform.rotate(
                child:new Container(
                width: 98,
                height: 98,
                decoration: BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/logo.png")
                  )
                ),
              ),
              angle: rotateAnimation.value,
              );  
  }
  @override
  Widget build(BuildContext context) {
    var page =MediaQuery.of(context).size;
    return new Scaffold(
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        child: const Icon(Icons.add),
        onPressed: () {},
      ),
      bottomNavigationBar: new BottomAppBar(
          notchMargin: 10.0,
          shape: CircularNotchedRectangle(),
          color: Colors.white,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              IconButton(icon: Icon(Icons.menu), onPressed: () {},),
            ],
          ),
        ),
      resizeToAvoidBottomPadding: false,
      body: new Container(
        child: new Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            new Container(
              height: page.height,
              width: page.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin:Alignment(0.0, -1),
                  end: Alignment(0, 0),
                  colors: [const Color(0xffb7dc53),const Color(0xff2aac7d)],
                  tileMode: TileMode.clamp
                )
              ),
            ),
            new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
                new AnimatedBuilder(
                animation: controller,
                builder: _animationBuilder,
                ),
               new Padding(
                child: new Text('اسپه',style: new TextStyle(fontSize: 20,color: Colors.white,fontWeight:FontWeight.bold)),
                padding: new EdgeInsets.fromLTRB(5,10,0,0)
              ),
              //   new Container(
              //   width: 350,
              //   height: 350,
              //   decoration: BoxDecoration(
              //     image: new DecorationImage(
              //       image: new AssetImage("assets/images/gife-khali.gif")
              //     )
              //   ),
              // ),
               new Padding(
                 
                child:new Opacity(
                  child: new Directionality(textDirection: TextDirection.rtl,
                  child:new Text('پیام شما دریافت شد و راننده در حال حرکت به سمت شماست',
                  textAlign: TextAlign.center,
                  style: new TextStyle(fontSize: 18,color: Colors.white,fontWeight:FontWeight.w100))
                  ),
                  opacity: _currentState,
                ),
                padding: new EdgeInsets.fromLTRB(0,20,5,0)
              ),
            ],
          ),       
          ],
        ),
      ),
    );
  }
  sendAciveReq() async{
    if(_currentState == 0.0){
    final response = await http.post('http://130.185.74.192:3000/api/v1/user/active',body: {"user_id":widget.data['_id'],"active":'true'});
    var responseBody = json.decode(response.body);
    print(_currentState);
     print(responseBody);
    if(!(responseBody == '')){
      setState(() {
        _currentbutton = 'لغو';
        _currentState = 1.0;
      });
    }
    }else{
    final response = await http.post('http://130.185.74.192:3000/api/v1/user/active',body: {"user_id":widget.data['_id'],"active":'false'});
    var responseBody = json.decode(response.body);
    print(_currentState);
    print(responseBody);
    if(!(responseBody == '')){
      setState(() {
        _currentbutton = '!بیا بِبَر';
        _currentState = 0.0;
      });
    }
    }
  }
  getUserActiveState() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('userToken');
    final response = await http.get('http://130.185.74.192:3000/api/v1/user/${apiToken}');
    var responseBody = json.decode(response.body);
    print('sadsaa2');
    print(responseBody['data']['activeRec']);
    if(!(responseBody['data'] == '')){
      if(responseBody['data']['activeRec'] == false){
         setState(() {
        _currentbutton = '!بیا بِبَر';
        _currentState = 0.0;
      });
      }else if(responseBody['data']['activeRec'] == true){
         setState(() {
        _currentbutton = 'لغو';
        _currentState = 1.0;
      });
      }
    } 
  }
}