import 'package:flutter/material.dart';
import 'dart:async';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:espe/components/drawer.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:pushe/pushe.dart';
class UserHome extends StatefulWidget {
  final data;
  const UserHome({this.data,Key key}):super(key:key);
  @override
  _UserHomeState createState() => _UserHomeState();
}

class _UserHomeState extends State<UserHome> with TickerProviderStateMixin {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
    var scoreT = '0';
   bool _currentActive = true;
   var _currentbutton = 'صبر کنید';
   double _currentState = 0.0;
   double _currentFontSize = 13;
   String nameD = '';
   String user_id;
   AnimationController controller;
   Animation<double> rotateAnimation;
   
   
   @override
  void initState() {
    super.initState();
    controller = new AnimationController(vsync: this,duration: const Duration(milliseconds: 4000));
    rotateAnimation = Tween(begin: 0.0,
    end: 12.6).animate(
    new CurvedAnimation(parent: controller,curve:new Interval(0.0,1,curve:Curves.elasticInOut)));

     Timer time2 = new Timer(new Duration(seconds: 2), (){
      getUserActiveState();
    });
  }
 Widget _animationBuilder(BuildContext context,Widget child){
    return new Container(
                width: 98,
                height: 98,
                decoration: BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/logoespe.png")
                  )
                ),
              );
  }
  @override
  Widget build(BuildContext context) {
    var page =MediaQuery.of(context).size;
    return new Scaffold(
      key: _scaffoldKey,
      endDrawer: SizedBox(child: buildDrawerLayout(context,nameD),width: 170),
      body:new Stack(
      children: <Widget>[
          new Material(
          child: new Container(
            color: Colors.grey,
            child:new Container(
              height: page.height,
              width: page.width,
              decoration: BoxDecoration(
               image: new DecorationImage(
                 image: new AssetImage("assets/images/espebg.png"),
                 fit: BoxFit.cover
               )
              ),
              // decoration: BoxDecoration(
              //   gradient: LinearGradient(
              //     begin:Alignment(0.0, -1),
              //     end: Alignment(0, 0),
              //     colors: [const Color(0xffb7dc53),const Color(0xff2aac7d)],
              //     tileMode: TileMode.clamp
              //   )
              // ),
              child: new Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
                new AnimatedBuilder(
                animation: controller,
                builder: _animationBuilder,
                ),
               new Padding(
                child: new Text('اِســپه',style: new TextStyle(fontSize: 20,color: Colors.white,fontWeight:FontWeight.bold)),
                padding: new EdgeInsets.fromLTRB(5,10,0,110)
              ),
              //   new Container(
              //   width: 350,
              //   height: 350,
              //   decoration: BoxDecoration(
              //     image: new DecorationImage(
              //       image: new AssetImage("assets/images/gife-khali.gif")
              //     )
              //   ),
              // ),
               new Padding( 
                child:new Column(
                  children: <Widget>[
                  new Opacity(
                  child: new Directionality(textDirection: TextDirection.rtl,
                  child:new Text('پیام شما دریافت شد ',
                  textAlign: TextAlign.center,
                  style: new TextStyle(fontSize: 18,color: Colors.white,fontWeight:FontWeight.w100))
                  ),
                  opacity: _currentState,
                ),
                new SizedBox(
                  height: 20,
                ),
                  new Opacity(
                  child: new Directionality(textDirection: TextDirection.rtl,
                  child:new Text(' راننده در حال حرکت به سمت شماست',
                  textAlign: TextAlign.center,
                  style: new TextStyle(fontSize: 18,color: Colors.white,fontWeight:FontWeight.w100))
                  ),
                  opacity: _currentState,
                ),
                  ],
                ),
                padding: new EdgeInsets.fromLTRB(0,20,5,0)
              ),
            ],
          ), 
              ),
            
                )
              ),
        new Positioned(
          child: new Container(
            margin: EdgeInsets.only(top:MediaQuery.of(context).size.height - 55),
            child: new Scaffold(
              backgroundColor: Colors.transparent,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: new Transform.scale(
        scale: 1.2,
        child: FloatingActionButton(
        backgroundColor:Color(0xff4FC172),
        elevation: 5,
        child:new Text(_currentbutton,style: new TextStyle(color: Color(0xffffffff),fontSize:14),),
        onPressed: () {
          if(_currentState == 1.0){
            controller.reverse();
          }else if(_currentState == 0.0){
            controller.forward();
          }
          Timer time2 = new Timer(new Duration(seconds:1), (){
              sendAciveReq();
              
          });
        },
      ),
      ),
      bottomNavigationBar: new BottomAppBar(
          notchMargin: 16.0,
          shape: CircularNotchedRectangle(),
          color: Colors.white,
          elevation: 20,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround, //spacebetween if we have 2 children
            children: <Widget>[
                    Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: Text('${scoreT}:امتیاز',style: new TextStyle(color:Color(0xff36b07a)),)
                ),
                     new Expanded(
                flex: 5,
                child:Container(
                  color: Colors.green,
                  width: 0,
                  height: 0,
                )
              ), 
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: new IconButton(
                    icon: Icon(Icons.menu,color:Color(0xff36b07a)),
                    color:Color(0xff36b07a),
                    iconSize: 35,
                    onPressed: (){
                      _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                ),
           
             ],
          ),
        )
          ),
        )
        )
        ],
    ),
    );
  }

  sendAciveReq() async{
    if(_currentState == 0.0){
    final response = await http.post('http://130.185.74.192:3000/api/v1/user/active',body: {"user_id":user_id,"active":'true'});
    final response2 = await http.get('http://130.185.74.192:3000/api/v1/users/npcodes');
    var responseBody = json.decode(response.body);
    var responseBody2 = json.decode(response2.body);
    print(_currentState);
     print(responseBody);
     //print(responseBody2);
    if(!(responseBody == '')){
      Pushe.sendSimpleNotifToUser(responseBody2['users']['npcode'], 'درخواست جدید برای اسپه', 'از طرف'+responseBody['name']);
      setState(() {
        _currentFontSize = 18;
        _currentbutton = 'لغو';
        _currentState = 1.0;
      });
    }
    }else{
    final response = await http.post('http://130.185.74.192:3000/api/v1/user/active',body: {"user_id":user_id,"active":'false'});
    var responseBody = json.decode(response.body);
    print(_currentState);
    print(responseBody);
    if(!(responseBody == '')){
      setState(() {
        _currentFontSize = 18;
        _currentbutton = '!بیا بِبَر';
        _currentState = 0.0;
      });
    }
    }
  }
  getUserActiveState() async{

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('userToken');
    //prefs.clear();
    final response = await http.get('http://130.185.74.192:3000/api/v1/user/${apiToken}');
    var responseBody = json.decode(response.body);
    var score = responseBody['data']['score'];
    var userid = responseBody['data']['_id'];
    print(userid);
    print(score);
    final response2 = await http.get('http://130.185.74.192:3000/api/v1/user/score/${userid}');
    var responseBody2 = json.decode(response2.body);
    var finalscore = (responseBody2['data'] * 20) + score;
    print(finalscore);
    if(!(responseBody['data'] == '' || responseBody['data'] == null)){
      await prefs.setString('userName',responseBody['data']['name']);
      await prefs.setString('Score',finalscore.toString());
      nameD = prefs.getString('userName');
      scoreT = prefs.getString('Score');
      user_id = responseBody['data']['_id'];
      if(responseBody['data']['activeRec'] == false){
         setState(() {
           _currentFontSize = 18;
        _currentbutton = '!بیا بِبَر';
        _currentState = 0.0;
      });
      }else if(responseBody['data']['activeRec'] == true){
         setState(() {
           _currentFontSize = 18;
        _currentbutton = 'لغو';
        _currentState = 1.0;
      });
      }
    } 
  }
}