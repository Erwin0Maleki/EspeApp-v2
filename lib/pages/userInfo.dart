import 'package:flutter/material.dart';
import 'package:espe/components/registerform.dart';
import 'package:espe/components/userInfoForm.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
// import 'package:url_launcher/url_launcher.dart';
class UserinfoView extends StatefulWidget {
  final data;
  const UserinfoView({@required this.data,Key key}):super(key:key);
  @override
  _UserinfoViewState createState() => _UserinfoViewState();
}

class _UserinfoViewState extends State<UserinfoView> {
  final formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  var text = 'ارسال';
  String _weightValue;

  weightOnSaved(String value){
    print(value);
    _weightValue =value;
  }
  changeActivetoFalse() async{
    final response = await http.post('http://130.185.74.192:3000/api/v1/user/active',body: {"user_id":widget.data['_id'],"active":'false'});
    var responseBody = json.decode(response.body);
  }
  sendWeight() async{
    setState(() {
     text='صبر کنید...'; 
    });
    final response = await http.post('http://130.185.74.192:3000/api/v1/user/addweight',body: {"amount":_weightValue,"user_id":widget.data['_id']});
    var responseBody = json.decode(response.body);
    final response2 = await http.post('http://130.185.74.192:3000/api/v1//user/addscore/${widget.data['_id']}',body: {"amount":_weightValue});
    var responseBody2 = json.decode(response2.body);
    print(responseBody2);
    if(responseBody['success'] == true){
          await changeActivetoFalse();
          await _scaffoldKey.currentState.showSnackBar(
          new SnackBar(
          duration: new Duration(minutes: 1),
          content:new Text('با موفقیت اضافه شد',style: new TextStyle(fontFamily: 'IRANSans'))
          )
        );
    Timer time2 = new Timer(new Duration(seconds: 2), (){
    Navigator.pop(context);
    });
    }else{
          await _scaffoldKey.currentState.showSnackBar(
          new SnackBar(
          duration: new Duration(minutes: 1),
          content:new Text('مشکلی وجود دارد لطفا اتصالات خود را بررسی کنید)',style: new TextStyle(fontFamily: 'IRANSans'))
          )
        );
    }
    

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
        title: new Text('اِسپه'),
        backgroundColor: Color(0xffb7dc53),
      ),
      resizeToAvoidBottomPadding: false,
      key: _scaffoldKey,
      body: new Container(
        color: Color(0xffb7dc53),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Text(widget.data['name'],style: TextStyle(color: Colors.white,fontSize: 18),)
            ),
            new GestureDetector(
                          child: Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Text(widget.data['phone'],style: TextStyle(color: Colors.white,fontSize: 18),)
              ),
              onTap: (){
                //launch('tel:09306613683');
              },
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: Text(widget.data['address'],style: TextStyle(color: Colors.white,fontSize: 18),textAlign: TextAlign.center,)
            ),
            Padding(
              padding: const EdgeInsets.only(top: 20),
              child: UserInfoForm(
                userInfoKey:formKey,
                weightOnSaved:weightOnSaved,
              ),
            ),
             new Padding(
                child:new GestureDetector(
                    onTap:() {
                      if(formKey.currentState.validate()) {
                        formKey.currentState.save();
                        FocusScope.of(context).requestFocus(new FocusNode());
                        sendWeight();
                      }
                      },
                  child:new Container(
                  width: 320,
                  height: 45,
                  alignment: Alignment.center,
                  child: new Text(text,style: new TextStyle(color:new Color(0xff41b476),fontWeight:FontWeight.w700,letterSpacing: .3)),
                  decoration: new BoxDecoration(color: Colors.white,borderRadius: BorderRadius.circular(25)),
                      )
                    ),
                padding: new EdgeInsets.fromLTRB(5,40,0,0)
              ),
          ],
        ),
      )
    );
  }
}