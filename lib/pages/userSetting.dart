import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:espe/components/drawer.dart';
import 'package:espe/components/settingForm.dart';
import 'package:espe/pages/learnV2.dart';
import 'package:espe/pages/userHome.dart';
import 'package:flutter/material.dart';
import 'package:espe/components/registerform.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UserSettingView extends StatefulWidget {
  final data;
  const UserSettingView({@required this.data,Key key}):super(key:key);
  @override
  _UserSettingViewState createState() => _UserSettingViewState();
}

class _UserSettingViewState extends State<UserSettingView> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final formKey = GlobalKey<FormState>();
  var scoreT = '0';
    String _phoneValue;
    String _passwordValue;
    String _nameValue;
    String _addressValue;
    String user_id;
    String nameD = '';
    var userName = 'صبر کنید';
    phoneOnSaved(String value){
    print(value);
    _phoneValue = value;
    }

    passwordOnSaved(String value){
    print(value);
    _passwordValue = value;
    }

    _nameOnSaved(String value){
    print(value);
    _nameValue = value;
    }

    _addressOnSaved(String value){
    print(value);
    _addressValue = value;
    }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserActiveState();
  }
  @override
  Widget build(BuildContext context) {
    var page =MediaQuery.of(context).size;
    return new Scaffold(
    appBar: new AppBar(
      backgroundColor: Colors.white,
      title:Padding(
        padding: const EdgeInsets.only(left: 0),
        child: Center(child: Text('مشخصات من',style: new TextStyle(color:Color(0xff36b07a)),)),
      ),
      
    ),
    key: _scaffoldKey,
    endDrawer: SizedBox(child: buildDrawerLayout(context,nameD),width: 170,),
    body:new Stack(
      children: <Widget>[
          new Material(
          child: new Container(
            color: Colors.red,
            child:new Container(
              height: page.height,
              width: page.width,
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  begin:Alignment(0.0, -1),
                  end: Alignment(0, 0),
                  colors: [const Color(0xffb7dc53),const Color(0xff2aac7d)],
                  tileMode: TileMode.clamp
                )
              ),
              child: ListView(
                padding: EdgeInsets.only(bottom: 100),
                children: <Widget>[
                  new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              new Container(
                width: 98,
                height: 98,
                margin: EdgeInsets.only(top: 25),
                decoration: BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/un2.png")
                  )
                ),
              ),
              // new Padding(
              //   padding: EdgeInsets.only(top: 15),
              //   child:settingForm(
              //     registerkey : formRegKey,
              //     phoneOnSaved:phoneOnSaved,
              //     passwordOnSaved:passwordOnSaved,
              //     nameOnSaved:_nameOnSaved,
              //     addressOnSaved:_addressOnSaved
              //     ),
              // )
              new Padding(
                padding: EdgeInsets.only(top: 15),
                child:Padding(
                  padding:  const EdgeInsets.symmetric(horizontal: 40,vertical: 10),
                  child: Form(
                    key: formKey,
                                      child: Column(
                      children: <Widget>[
                        Stack(
                          alignment: Alignment.centerRight,
                          children: <Widget>[ 
                            Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: Text(':نام و نام خانوادگی',textAlign: TextAlign.right,style: TextStyle(color: Colors.white54),),
                            ),
                            new TextFormField(
                            enabled: true,
                            textAlign: TextAlign.left,
                            onSaved: _nameOnSaved,
                            obscureText: false,
                            validator: (String value){
                              if(value.length<1){
                                return 'معتبر نیست';
                              }
                            },
                            initialValue: widget.data['name'],
                            style:const TextStyle(
                              color: Colors.white,
                              
                            ),
                            decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white,width: 2.0),
                                borderRadius: BorderRadius.circular(25.0)
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white,width: 2.0),
                                borderRadius: BorderRadius.circular(25.0)
                              ),
                              hintText: "نام و نام خانوادگی",
                              errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.red,width: 2.0),
                                borderRadius: BorderRadius.circular(25.0)
                              ),
                              focusedErrorBorder:OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.red,width: 2.0),
                                borderRadius: BorderRadius.circular(25.0)
                              ),
                              hintStyle: const TextStyle(color:Colors.white,fontSize: 15,),
                              contentPadding: const EdgeInsets.only(top:12,bottom: 12,left: 15,right:15)
                            ),
                          ),
                          ]
                        ),
                        Stack(
                          alignment: Alignment.centerRight,
                          children: <Widget>[ 
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 10, 10, 0),
                              child: Text(':تلفن همراه',textAlign: TextAlign.right,style: TextStyle(color: Colors.white54),),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: new TextFormField(
                              enabled: true,
                                                        validator: (String value){
                              if(value.length<1){
                                return 'معتبر نیست';
                              }
                            },
                              textAlign: TextAlign.left,
                              obscureText: false,
                              onSaved: phoneOnSaved,
                              initialValue: widget.data['phone'],
                              style:const TextStyle(
                                color: Colors.white,
                                
                              ),
                              decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white,width: 2.0),
                                  borderRadius: BorderRadius.circular(25.0)
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.white,width: 2.0),
                                  borderRadius: BorderRadius.circular(25.0)
                                ),
                                hintText: "تلفن همراه",
                                errorBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.red,width: 2.0),
                                  borderRadius: BorderRadius.circular(25.0)
                                ),
                                focusedErrorBorder:OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.red,width: 2.0),
                                  borderRadius: BorderRadius.circular(25.0)
                                ),
                                hintStyle: const TextStyle(color:Colors.white,fontSize: 15,),
                                contentPadding: const EdgeInsets.only(top:12,bottom: 12,left: 15,right:15)
                              ),
                          ),
                            ),
                      ]
                        ),
                        Stack(
                          alignment: Alignment.centerRight,
                          children: <Widget>[ 
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 10, 10, 0),
                              child: Text(':گذرواژه',textAlign: TextAlign.right,style: TextStyle(color: Colors.white54),),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child:  new TextFormField(
                            enabled: true,
                                                      validator: (String value){
                              if(value.length<1){
                                return 'معتبر نیست';
                              }
                            },
                            textAlign: TextAlign.left,
                            onSaved: passwordOnSaved,
                            obscureText: true,
                            initialValue: widget.data['password'],
                            style:const TextStyle(
                              color: Colors.white,
                              
                            ),
                            decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white,width: 2.0),
                                borderRadius: BorderRadius.circular(25.0)
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white,width: 2.0),
                                borderRadius: BorderRadius.circular(25.0)
                              ),
                              hintText: "گذرواژه",
                              errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.red,width: 2.0),
                                borderRadius: BorderRadius.circular(25.0)
                              ),
                              focusedErrorBorder:OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.red,width: 2.0),
                                borderRadius: BorderRadius.circular(25.0)
                              ),
                              hintStyle: const TextStyle(color:Colors.white,fontSize: 15,),
                              contentPadding: const EdgeInsets.only(top:12,bottom: 12,left: 15,right:15)
                            ),
                          ),
                            )]
                        ),
                        Stack(
                          alignment: Alignment.centerRight,
                          children: <Widget>[ 
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0, 10, 10, 0),
                              child: Text(':آدرس',textAlign: TextAlign.right,style: TextStyle(color: Colors.white54),),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child:  new TextFormField(
                            enabled: true,
                                                      validator: (String value){
                              if(value.length<1){
                                return 'معتبر نیست';
                              }
                            },
                            textAlign: TextAlign.left,
                            maxLines: 3,
                            onSaved: _addressOnSaved,
                            initialValue: widget.data['address'],
                            style:const TextStyle(
                              color: Colors.white,
                              
                            ),
                            decoration: InputDecoration(
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white,width: 2.0),
                                borderRadius: BorderRadius.circular(25.0)
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.white,width: 2.0),
                                borderRadius: BorderRadius.circular(25.0)
                              ),
                              hintText: "ادرس",
                              errorBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.red,width: 2.0),
                                borderRadius: BorderRadius.circular(25.0)
                              ),
                              focusedErrorBorder:OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.red,width: 2.0),
                                borderRadius: BorderRadius.circular(25.0)
                              ),
                              hintStyle: const TextStyle(color:Colors.white,fontSize: 15,),
                              contentPadding: const EdgeInsets.only(top:12,bottom: 12,left: 15,right:15)
                            ),
                          ),
                            )]
                        ),
                      ],
                    ),
                  ),
                )
              ),
                  GestureDetector(
                            child: Container(
                            width: 50,
                            padding: EdgeInsets.all(3),
                              decoration: new BoxDecoration(
                     borderRadius: BorderRadius.circular(20),
                      color: Colors.white,
                   ),
                            child: Center(child: new Text('ثبت',style: TextStyle(color:Color(0xff36b07a)),))
                          ),
                          onTap: (){
                          if(formKey.currentState.validate()) {
                          formKey.currentState.save();
                          user_id = widget.data['_id'];
                          sendupdate();
                        }
                          },
                        )      
            ],
          ),  
                ],
              )
              ),
            
                )
              ),
        new Positioned(
          child: new Container(
            margin: EdgeInsets.only(top:MediaQuery.of(context).size.height - 155),
            child: new Scaffold(
              backgroundColor: Colors.transparent,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        backgroundColor:Color(0xff43d1c5),
        elevation: 5,
        child:new Icon(Icons.home),
        onPressed: (){
         Navigator.push(
         context,
         new MaterialPageRoute(builder: (context) =>  new Directionality(textDirection: TextDirection.ltr,child:UserHome() ,)));
        }
      ),
      bottomNavigationBar: new BottomAppBar(
          notchMargin: 10.0,
          shape: CircularNotchedRectangle(),
          color: Colors.white,
          elevation: 20,
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween, //spacebetween if we have 2 children
            children: <Widget>[
                          Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: Text('${scoreT}:امتیاز',style: new TextStyle(color:Color(0xff36b07a)),)
                ),
                  Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 19),
                  child: new IconButton(
                    icon: Icon(Icons.menu,color:Color(0xff36b07a)),
                    color:Color(0xff36b07a),
                    iconSize: 35,
                    onPressed: (){
                      _scaffoldKey.currentState.openEndDrawer();
                    },
                  ),
                ),
             ],
          ),
        )
          ),
        )
        )
        ],
    ),
    );
  }
   getUserActiveState() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String apiToken = prefs.getString('userToken');
    final response = await http.get('http://130.185.74.192:3000/api/v1/user/${apiToken}');
    print(apiToken);
    var responseBody = json.decode(response.body);
    if(!(responseBody['data'] == '' || responseBody['data'] == null)){
      await prefs.setString('userName',responseBody['data']['name']);
      nameD = prefs.getString('userName');
      scoreT = prefs.getString('Score');
      print(responseBody);
      setState(() {
       userName=responseBody['data']['name'];
      });
    } 
  }
   sendupdate() async{
    final response = await http.post('http://130.185.74.192:3000/api/v1/user/updatedata',body: {"user_id":user_id,"name":_nameValue,"password":_passwordValue,"phone":_phoneValue,"address":_addressValue});
    var responseBody = json.decode(response.body);
    print(responseBody);
      if(responseBody['success'] == true){
        _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          content: new Text(
          'اطلاعات شما بروز شد',
          style: new TextStyle(fontFamily: 'IRANSans'),
          textAlign: TextAlign.right,
        ),
        )
      );
    } else {
        _scaffoldKey.currentState.showSnackBar(
        new SnackBar(
          content: new Text(
          "خطا",
          style: new TextStyle(fontFamily: 'IRANSans'),
          textAlign: TextAlign.right,
        ),
        )
      );
    }
   
  }
}