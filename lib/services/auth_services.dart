import 'package:http/http.dart' as http;
import 'dart:convert';
class AuthServices {
    Future<Map> sendDataForLogin(Map body) async {
      print('in auth');
      final response = await http.post('http://130.185.74.192:3000/api/v1/login',body: body);
      var responseBody = json.decode(response.body);
      return responseBody;
    }
}